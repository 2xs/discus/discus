# DISCUS - A Intrusion detection architecture using DSL-based configuration

DISCUS is a software developped by the [2XS](https://cristal.univ-lille.fr/2XS) team of the 
[CRIStAL](https://cristal.univ-lille.fr) Laboratory of [Université de Lille](https://www.univ-lille.fr)

## What is it ?
Nowadays, cloud computing becomes quite popular and a lot of research is
done on services it provides. Most of security challenges induced by
this new architecture are not yet tackled. In this work, we propose a
new security architecture, based on a massively distributed network of
security solutions, to address these challenges. Current solutions, like
IDS or firewalls, were not formerly designed to detect attacks that draw
profit from the cloud structure.

Our solution DISCUS is based on a distributed architecture using both
physical and virtual probes, along with former security solutions (IDS
and firewalls). DISCUS is driven by DISCUS SCRIPT, a dedicated language
that provides an easy way to configure the components of our solution.

The related research paper are:

1. Damien Riquet, Gilles Grimaud, Michaël Hauspie. *Large-scale coordinated attacks : Impact on the cloud security*. The Second International Workshop on Mobile Commerce, Cloud Computing, Network and Communication Security 2012, Jul 2012, Palermo, Italy. pp.558. [⟨hal-00723739⟩](https://hal.archives-ouvertes.fr/hal-00723739v1)
2. Damien Riquet, Gilles Grimaud, Michaël Hauspie. *DISCUS: A massively distributed IDS architecture using a DSL-based configuration*. International Conference on Information Science, Electronics and Electrical Engineering, Apr 2014, Sapporo, Japan. 5 p. [⟨hal-00996876⟩](https://hal.archives-ouvertes.fr/hal-00996876v1)
3. Damien Riquet. [*DISCUS : une architecture de détection d'intrusions réseau distribuée basée sur un langage dédié*](https://theses.fr/2015LIL10207). PhD Thesis, Université de Lille, 2015. (in french)



## Preparing your environment

In order to work with `DISCUS`, you will need:

- A valid _C_ compilation environment, depending on your platform target:
  - for target _x84_64-linux-gnu_ you will need: __gcc__ (4.9 or above) or __clang__ (3.5 or above), __GNU Make__ (4.0 or above)
  - for target _x86_64-apple-darwin_ you will need: __clang__ (7.3.0 or above) and __GNU Make__ (3.8 or above)
  - for target _x86_64-portbld-freebsd10.1_ you will need: __gcc__ (4.8.5 or above) and __GNU Make__ (4.1 or above)
  - Libpcap (0.8 or above)
  - libpcre-dev (3.0 or above)
  - Python version 2.7 or above
  - a `requirements.txt` file is provided to install needed python
    dependencies. Use `pip install -r requirements.txt` to install
    those dependencies. You probably should use `virtualenv` to setup
    a discus specific python environment.

The `DISCUS` compilation process has been tested on Linux and Mac OS X.

## Waht's in the package?

- ___build/___
  - ___gen/___ - Contains each generated files by convert rules.
  - ___lib/___ - Contains pre-compile file from platform depedante functions
  - ___object/___ - Contains each object file from core/
  - ___rules/___ - Contains the convert rules
- ___examples/___ - Contains a few sample entries `discus` and `snort scripts`.
- ___src/___
  - ___compiler/___ - Contains the discus code generations tool to genrate `c` code
  - ___core/___ - `DISCUS` kernel C source code (portable files).
  - ___platform/___ - DISUCS port to different devices.
- ___includes/___
   - ___private/___ - Contains each header only used by core/
    - ___public/___ - Contains each header used bith by gen/ and core/
- ___tools/___
  - ___memory_stats/___ - Generations statistics.
  - ___mutation/___  - Allow to tansfer a source file. 
  - ___snort2discus/___ - Snort rules translator in discus script.
  - ___exp/___  - Files used for experiments on mutations.

## Compiling `DISCUS`
When building `DISCUS` you have to specify target and `DISCUS Script` rules to be build with it.

For example to build `DISICUS` for a destination target _Linux_ and `DISCUS Script` called _rules.discus_
you can type:
```{r, engine='bash', build_discus}
$ make RULES=rules.discus TARGET=Linux
```
### `DISCUS` compilation options
- TARGET=X
    - target on witch to compile `DISCUS`.
- RULES=Z
    - The `DISCUS Script` file.
- DEBUG=true|false
    - Enable or disable debug mode.

To clean built files type:
```{r, engine='bash', built_clean}
$ make clean TARGET=Linux
```

## Installing

### On POSIX
#### Reading pcap files
```{r, engine='bash', read_pcap}
$ ./main --pcap-single=path/to/file.pcap
```

#### Listen on interface
```{r, engine='bash', listen_interface}
$ ./main --iface=<eth0>
```

## Profilling `DISCUS`

## KCacheGrind

```{r, engine='bash', usage_kcachegrind}
$ valgrind --tool=callgrind -v EXECUTABLE
```

## Troubleshooting
- Unable to do LD on FreeBSD.

## F.A.Q

Q: I would like to make a discus port on a custom platform ?

A: You must write the functions described in _src/platform/includes_.
    Each headers files contains the prototypes you must impletment in corresponding source file.
    
## Authors
### Original author
* Damien Riquet 

### Maintainers 
* Michael Hauspie (main contact)
* Gille Grimaud

### Additional contributors
* Edward Delbar
* Clément Boin
