BASE := $(PWD)
ifndef TARGET
    $(error TARGET is not set. Please set to the target for which you want compile.)
endif

include src/platform/$(TARGET)/Makefile.vars

# General Makefile
# Usage: make file.discus
CFLAGS := -g $(WARNINGS)
CFLAGS += -Isrc/core/ -Isrc/platform -O3
LDFLAGS = -g -lpcap -lpcre -L$(LIBDIR) -lplatform -lgenerate
DISCUSC := src/compiler/dc.py
MKDIR_P := mkdir -p

INCDIR := includes/
COREDIR := src/core
RULESDIR := build/rules
CTXDIR := src/compiler
BUILDDIR := build
GENDIR := build/gen
OBJDIR := build/objects
LIBDIR := build/lib
DOCDIR := doc
PLATFORMDIR := src/platform/$(TARGET)
PLATFORMINCDIR := src/platform/includes

CORESRCS := $(wildcard $(COREDIR)/*.c)
COREOBJS := $(patsubst %.c, %.o, $(CORESRCS))
GENSRC := $(wildcard $(GENDIR)/*.c)
GENHDRS := $(wildcard $(GENDIR)/*.h)
GENOBJS := $(patsubst %.c, %.o, $(GENSRC))
OBJGEN := $(wildcard $(GENDIR)/*.o)

# Generation of the context file
all: main

# $(OBJDIR)/table.o
main: $(LIBDIR)/libgenerate.a $(OBJDIR)/event.o $(OBJDIR)/queue.o $(OBJDIR)/types.o $(OBJDIR)/engine.o $(OBJDIR)/alert.o $(LIBDIR)/libplatform.a
	$(CC) $(CFLAGS) $(OBJDIR)/*.o $(LDFLAGS) -o main

$(LIBDIR)/libgenerate.a: $(patsubst %.discus, %.ctx, $(RULES))
	cd $(CTXDIR) &&  python main.py --input $(BASE)/$(RULESDIR)/$(notdir $<) $(BASE)/$(GENDIR)
	@cd $(BASE)
	make -C $(BUILDDIR) TARGET=$(TARGET) BASE=$(BASE)
	ranlib $@

$(LIBDIR)/libplatform.a:
	make -C $(PLATFORMDIR)

%.ctx: %.discus
	$(shell python $(DISCUSC) --output $(RULESDIR)/$(notdir $@) $<)

$(OBJDIR)/engine.o: $(COREDIR)/engine.c $(COREDIR)/engine.h $(COREDIR)/queue.h $(COREDIR)/types.h   $(COREDIR)/core.h
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/types.o: $(COREDIR)/types.c $(COREDIR)/types.h
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/queue.o: $(COREDIR)/queue.c $(COREDIR)/queue.h $(COREDIR)/event.h
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/event.o: $(COREDIR)/event.c $(COREDIR)/types.h $(COREDIR)/event.h
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/main.o: $(COREDIR)/main.c $(COREDIR)/engine.h
	$(CC) $(CFLAGS) -Isrc/core/platform/includes -c $< -o $@

$(OBJDIR)/alert.o: $(COREDIR)/alert.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/%.o: $(GENDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

doc: FORCE
	doxygen $(DOCDIR)/Doxyfile
	@echo "Generate doc !"

clean:
	$(RM) -r $(DOCDIR)/html
	$(RM) $(OBJDIR)/*.o
	$(RM) $(GENDIR)/*
	$(RM) main
	$(RM) $(RULESDIR)/*
	make -C $(BUILDDIR) BASE=$(BASE) clean
	make -C $(PLATFORMDIR) clean

FORCE:
