Jinja2==2.8
MarkupSafe==0.23
argparse==1.2.1
ply==3.8
wsgiref==0.1.2
z3c.dependencychecker==1.15
