'''
File: field.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: A Field is a variable with a specific type
'''

import node

class Field(node.DiscusNode):
    def __init__(self, raw_name, type):
        super(Field, self).__init__()

        self.name = raw_name.value
        self.type = type

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))
