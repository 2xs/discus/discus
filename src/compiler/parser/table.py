'''
File: table.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Description of a table
'''

import node
from event import Event
from field import Field

class Table(node.DiscusNode):
    """
        A table is a data structure
        It is composed of one or several fields
    """

    def __init__(self, raw_name, fields, context, lineno):
        """
            Initialize this table
                name: name of the table
                fields: list of (type, name(s) of field)
        """
        super(Table, self).__init__()
        self.set_lineno(lineno)
        self.name = raw_name.value
        self.fields_type = {}
        self.purge_stmt = None
        self.remove_stmt = None

        # Constructing fields
        self.fields = fields
        self.fields_name = [field.name for field in fields]

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))

        # Initialize fields
        for field in self.fields:
            if field.name in self.fields_type:
                context.add_error(self.error_message("Table '%s' has several fields entitled '%s'" % (name, field.name)))
            else:
                self.fields_type[field.name] = field.type


    def verify_node(self, context):
        for stmt in ['purge_stmt', 'remove_stmt']:
            stmt_attr = getattr(self, stmt)
            if stmt_attr is not None:
                entry_name, expression = stmt_attr
                event = Event(None, [], 0)
                event.add_set_identifier(entry_name, self.name)
                expression.verify_node(context, event)

        return True



    def set_purge_stmt(self, entry_name, expression):
        self.purge_stmt = (entry_name, expression)


    def set_remove_stmt(self, entry_name, expression):
        self.remove_stmt = (entry_name, expression)


    def get_expressions_of_type(self, exp_class):
        exps = []

        for stmt_name in ['purge_stmt', 'remove_stmt']:
            stmt = getattr(self, stmt_name)
            if stmt is not None:
                exps.extend(stmt[1].get_expressions_of_type(exp_class))

        return exps
