'''
File: type.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: This file describes all the type used by Discus language

DiscusType is the abstract class of all Discus's types.
There are several types:
    - Bitstream : stream of bits with unknown size
    - Integer with known binary length
    - Float
    - Time
    - Enum
    - Undefined (undefined values)

'''

import unittest
from math import ceil, log

class DiscusType(object):
    """ Abstract type """

    def get_name(self):
        return self.__class__.__name__.capitalize()

    def match_type(self, other_type):
        return isinstance(other_type, self.__class__)

class Undefined(DiscusType):
    pass


class Bitstream(DiscusType):
    """ Bitstream type (sequence of bits) """
    pass


class Integer(DiscusType):
    """ Integer type with dynamic size """
    def __init__(self, size):
        self.size = size



class Float(DiscusType):
    """ Float type """
    pass


class Time(DiscusType):
    """ Time type """
    pass


class AnyEnumType(DiscusType):
    def match_type(self, other_type):
        return isinstance(other_type, EnumType) or isinstance(other_type, AnyEnumType)

    def get_name(self):
        return "Enum"


class EnumType(DiscusType):
    """ Enum type """
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return 'enum %s' % self.name

    def match_type(self, other_type):
        if isinstance(other_type, AnyEnumType):
            return True

        return super(EnumType, self).match_type(other_type) and self.name == other_type.name


class NetAddr(DiscusType):
    """ Represents the network address of a host """
    pass


def get_binary_length(val):
    """ Return the binary size necessary to store this decimal value """
    if val == 0: return 1
    return int(ceil(log(abs(val))/log(2)))


class TestFunctions(unittest.TestCase):

    def test_binary_length(self):
        self.assertEqual(get_binary_length(14), 4)
        self.assertEqual(get_binary_length(16), 4)
        self.assertEqual(get_binary_length(1024), 10)
        self.assertEqual(get_binary_length(1023), 10)


    def test_match_types(self):
        self.assertTrue(Integer(1).match_type(Integer(1)))
        self.assertFalse(Integer(1).match_type(Bitstream()))

        self.assertTrue(EnumType('test').match_type(EnumType('test')))
        self.assertFalse(EnumType('test').match_type(EnumType('anothertype')))
        self.assertTrue(EnumType('test').match_type(AnyEnumType()))
        self.assertTrue(AnyEnumType().match_type(EnumType('test')))


if __name__ == '__main__':
    unittest.main()
