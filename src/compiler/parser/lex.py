'''
# TODO
File: lex.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: This file contains lexer data about Discus language
'''

from ply.lex import TOKEN
import sys


# 1) First, we need to define the tokens of the language
# -------------------------------------------------------------------
reserved = {
    # Table related tokens
    'table'  : 'TABLE',
    'remove' : 'REMOVE',
    'from'   : 'FROM',
    'when'   : 'WHEN',
    'purge'  : 'PURGE',
    'select' : 'SELECT',
    # Event related tokens
    'where'     : 'WHERE',
    'on'        : 'ON',
    'raise'     : 'RAISE',
    'in'        : 'IN',
    'insert'    : 'INSERT',
    'into'      : 'INTO',
    'update'    : 'UPDATE',
    'let'       : 'LET',
    'alert'     : 'ALERT',
    # Logical operators
    'or'        : 'OR',
    'and'       : 'AND',
    'not'       : 'NOT',
    # Type specifier
    'float'     : 'FLOAT_TYPE',
    'time'      : 'TIME_TYPE',
    'enum'      : 'ENUM_TYPE',
    'bitstream' : 'BITSTREAM_TYPE',
    'ipaddr'    : 'IPADDR_TYPE',
    # Constants
    'now'       : 'NOW',
    'true'      : 'TRUE',
    'false'     : 'FALSE',
    # Set operation
    'exists'    : 'EXISTS',
    'for'       : 'FOR',
    'all'       : 'ALL',
    'first'     : 'FIRST',
    'with'      : 'WITH',
    'ifnone'    : 'IFNONE',
    # Match expression
    'match'     : 'MATCH',
    'as'        : 'AS',
    # Util functions
    'asbig'     : 'ASBIG',
    'aslittle'  : 'ASLITTLE',
    'len'       : 'LEN',
    'net4'      : 'NET4',
    'net6'      : 'NET6',
}

tokens = [
    # Naming and string
    'STRING', 'QUOTED_STRING',
    # Type specifier
    'INTEGER_TYPE',
    # Constant values
    'INT_CST', 'HEXA_CST', 'FLOAT_CST', 'IPv4_CST',
    # Operators
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'DOT', 'CONCAT',
    # Comparators
    'OP_EQ', 'OP_NE', 'OP_GT', 'OP_LT', 'OP_GE', 'OP_LE', 

]

# Literals are 1-character tokens
literals = "()[]{}:;,=!<>"


# 2) Second, we need to describe each token and how it is represented
# -------------------------------------------------------------------

# -------- BASIC TOKENS -------

# Operators
t_PLUS   = r'\+'
t_MINUS  = r'\-'
t_TIMES  = r'\*'
t_DIVIDE = r'\/'
t_DOT    = r'\.'
t_CONCAT = r'\.\.'

# Comparators
t_OP_EQ = r'\=\='
t_OP_NE = r'!\='
t_OP_GT = r'<'
t_OP_LT = r'>'
t_OP_GE = r'<\='
t_OP_LE = r'>\='

# Comments and ignored chars
t_ignore_COMMENT = r'\#.*'
t_ignore = " \t"

# -------- FUNCTION TOKENS -------

# Basic description 
digit        = r'[0-9]'
hexdigit     = r'[0-9a-fA-F]'
intstring    = digit + r'+'
hexstring    = r'0[xX]' + hexdigit + r'+'
letter       = r'[a-zA-Z]'
string       = letter + r'(%s|%s|[\-_])*' % (letter, digit)
quotedstring = r'\"[^"]*\"'



# Integer type Token
# Example: int2, int16 and so on
int_keyword = r'int'
int_type_token = int_keyword + intstring

@TOKEN(int_type_token)
def t_INTEGER_TYPE(t):
    t.value = int(t.value[len(int_keyword):])
    return t


# IPv4 Token
# Example: 192.168.2.1
# Warning: invalid ipv4 address will match this token
# Semantic layer will deal with this issue
ipv4_dec_addr = r'\.'.join([r'\d{1,3}'] * 4)

@TOKEN(ipv4_dec_addr)
def t_IPv4_CST(t):
    t.value = [int(b) for b in t.value.split('.')]
    return t


# Float Token
def t_FLOAT_CST(t):
    r'-?[0-9]+\.[0-9]+'
    t.value = float(t.value)
    return t


# Hexa integer Token
@TOKEN(hexstring)
def t_HEXA_CST(t):
    return t


# Decimal integer Token
@TOKEN(intstring)
def t_INT_CST(t):
    t.value = int(t.value)
    return t


# String
@TOKEN(quotedstring)
def t_QUOTED_STRING(t):
    return t

# STRING / IDENTIFIER
@TOKEN(string)
def t_STRING(t):
    t.type = reserved.get(t.value, 'STRING') # Check for reserved words
    return t


# Newline management
def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count('\n')


# Unknown token
def t_error(t):
    print "Illegal character \"%s\"" % t.value[0]
    sys.exit(1)


# Verify that every tokens has been defined
for token in tokens:
    token_def = "t_%s" % token

    if token_def not in globals():
        print "%s has not been defined" % token

# Add reserved words to tokens
tokens += reserved.values()


# At last, we can build the lexer
# -------------------------------------------------------------------
import ply.lex as lex
lex.lex()
