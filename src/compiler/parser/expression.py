'''
File: expression.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Describe Discus expression
'''

import re
import sys
from type import *
import node
import event as event_mod

# ----- ABSTRACT CLASS -----

class Expression(node.DiscusNode):
    """ Abstract class to represent an expression """

    def __init__(self, lineno):
        super(Expression, self).__init__()
        self.set_lineno(lineno)

    def verify_node(self, global_context, event):
        """
            Abstract method that every inherited classes must override
            This method verify that an expression is valid
        """
        sys.stderr.write('%s.verify_node might not be implanted yet\n' % self.__class__.__name__)

    def get_type(self, context, event):
        """
            Abstract method that every inherited classes must override
            This method returns the expression's type
        """
        sys.stderr.write('%s.get_type might not be implanted yet (%d)\n' % (self.__class__.__name__, self.lineno))
        return Undefined()


    def get_expressions_of_type(self, exp_class):
        """
            Abstract method that returns sub-expressions of the exp_class type
            This method returns a list of expressions
        """
        sys.stderr.write('%s.get_expressions_of_type might not be implanted yet\n' % self.__class__.__name__)
        return []


    def check_expression_of_type(self, exp_class, siblings=[]):
        """
            It checks whether or not this expression is an instance of exp_class
            If it is not, it tries with its siblings
        """
        if isinstance(self, exp_class):
            return [self]

        exps = []
        for sibling in siblings:
            exps.extend(sibling.get_expressions_of_type(exp_class))
        return exps


    def match_type(self, expression_type, expected_type, context, event):
        """ Return whether or not an expression match an expected type """
        return expression_type.match_type(expected_type)


    def test_type(self, expression, expected_type, context, event):
        expression_type = expression.get_type(context, event)

        if not self.match_type(expression_type, expected_type, context, event):
            error_msg = 'Incorrect type "%s" for "%s" expression (expected type "%s")'
            context.add_error(error_msg % (expression_type.get_name(), self.__class__.__name__, expected_type.get_name()), expression.lineno)
            return False
        return True


    def test_types(self, expression, expected_types, context, event):
        error_msg = 'Incorrect type "%s" for "%s" expression (expected types are: %s)'
        expected_types_str = ', '.join([exp_type.get_name() for exp_type in expected_types[:-1]])
        expected_types_str += ' or ' + expected_types[-1].get_name()

        expression_type = expression.get_type(context, event)

        correct_type = True in [self.match_type(expression_type, expected_type, context, event) for expected_type in expected_types]

        if not correct_type:
            context.add_error(error_msg % (expression_type.get_name(), self.__class__.__name__, expected_types_str), expression.lineno)
            return False

        return True


# ----- BASIC EXPRESSIONS -----

class ConstantExpression(Expression):
    """
        A Constant expression could be an integer, a float, a bitstream, an enum value,
        a timestamp, and so on
    """
    def __init__(self, constant_type, constant_value, raw_value, lineno):
        super(ConstantExpression, self).__init__(lineno)
        self.type = constant_type
        self.value = constant_value

        # Update metadata
        self.set_metadata('value', self.get_metadata_from(raw_value))

    def verify_node(self, context, event):
        # A constant value is always valid
        pass

    def get_type(self, context, event):
        return self.type

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)


class AnonymEnumExpression(Expression):
    """
        A constant list expression consists in a list of constant values of the same type
    """
    def __init__(self, constant_values, lineno):
        super(AnonymEnumExpression, self).__init__(lineno)
        self.values = [val.value for val in constant_values]
        self.raw_values = constant_values

    def verify_node(self, context, event):
        pass

    def get_type(self, context, event):
        return AnyEnumType()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)


class GlobalVariable(Expression):
    def __init__(self, raw_name, value, lineno):
        super(GlobalVariable, self).__init__(lineno)
        self.name = raw_name.value
        self.value = value

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))


    def verify_node(self, context, event):
        if context.exists_identifier(self.name):
            context.add_error('Identifier "%s" has been already declared' % self.name, self.lineno)
        else:
            context.add_var_identifier(self.name, self.value)

        # Then verify the value
        self.value.verify_node(context, event)

    def get_type(self, context, event):
        return self.value.get_type(context, event)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)


class Variable(GlobalVariable):
    def __init__(self, raw_name, value, lineno):
        super(Variable, self).__init__(raw_name, value, lineno)

    def verify_node(self, context, event):
        # First, verify that the name is available
        if is_declared_identifier(self.name, context, event):
            context.add_error('Identifier "%s" has been already declared' % self.name, self.lineno)
        else:
            event.add_var_identifier(self.name, self.value)

        # Then verify the value
        self.value.verify_node(context, event)


class MinusExpression(Expression):
    """
        Represent a unary minus operator
        The expression has to be an integer, a float or a time
        For example : - 1 or - - 2 or - ( x + 4) are valid expressions
    """
    def __init__(self, expression, lineno):
        super(MinusExpression, self).__init__(lineno)
        self.exp = expression


class NotExpression(Expression):
    """
        Represent a unary not operator
        The expression has to be an integer, a float or a time
    """
    def __init__(self, expression, lineno):
        super(NotExpression, self).__init__(lineno)
        self.exp = expression

    def verify_node(self, context, event):
        self.exp.verify_node(context, event)
        self.test_type(self.exp, Integer(1), context, event)

    def get_type(self, context, event):
        return self.exp.get_type(context, event)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.exp])


class IdentifierExpression(Expression):
    """
        Represent an identifier expression
        Could represent an argument, a table entry, a match extracted data or a global variable
    """
    def __init__(self, raw_name, lineno):
        super(IdentifierExpression, self).__init__(lineno)
        self.name = raw_name.value

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))

    def verify_node(self, context, event):
        if not is_declared_identifier(self.name, context, event):
            context.add_error('Undeclared variable %s' % self.name, self.lineno)

    def get_type(self, context, event):
        return get_identifier_type(self.name, context, event)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)


class RecordAccess(Expression):
    """
        Represent a field access to a record (a table or an enum)
    """
    def __init__(self, raw_container, raw_name, lineno):
        super(RecordAccess, self).__init__(lineno)
        self.container = raw_container.value
        self.name = raw_name.value

        # Update Metadata
        self.set_metadata('container', self.get_metadata_from(raw_container))
        self.set_metadata('name', self.get_metadata_from(raw_name))


    def verify_node(self, context, event):
        # A record could be from a table or from an enum

        if self.container in context.enums:
            if self.name not in context.enums[self.container].keys():
                context.add_error('Enum "%s" does not have a field entitled "%s"' % (self.container, self.name), self.lineno)

        elif self.container in event.set_identifiers:
                # container correspond to a set identifier
                table_name = event.set_identifiers[self.container]

                # first verify that the table exists ...
                if table_name not in context.tables:
                    context.add_error('table "%s" does exist' % (table_name), self.lineno)
                else:
                    table = context.tables[table_name]
                    if self.name not in table.fields_type:
                        context.add_error('Table "%s" does not have a field entitled "%s"' % (self.container, self.name), self.lineno)

        else:
            # Not from a table/enum
            context.add_error('Unknown variable or enum %s' % (self.container), self.lineno)

    def get_type(self, context, event):
        # A record access could represent a table or an enum field
        if self.container in context.enums:
            return EnumType(self.container)

        elif event is not None and self.container in event.set_identifiers:
            table_name = event.set_identifiers[self.container]

            # first verify that the table exists ...
            if table_name not in context.tables:
                context.add_error('table "%s" does exist' % (table_name), self.lineno)
            else:
                if self.name in context.tables[table_name].fields_type:
                    return context.tables[table_name].fields_type[self.name]

        return Undefined()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)


# ----- BITSTREAM RELATED EXPRESSIONS -----

class BitstreamExtractBit(Expression):
    """
        Represent an extraction of one bit from a bitstream
    """
    def __init__(self, name, index, lineno):
        super(BitstreamExtractBit, self).__init__(lineno)
        self.name = name
        self.index = index


    def verify_node(self, context, event):
        # We need to verify :
        # 1) the variable exists and corresponds to a bitstream
        self.test_type(self.name, Bitstream(), context, event)

        # 2) the index value is an integer
        self.test_type(self.index, Integer(1), context, event)


    def get_type(self, context, event):
        return Integer(1)


class BitstreamExtractBitstream(Expression):
    """
        Represent an extraction of a subset of bits from a bitstream
    """
    def __init__(self, name, lineno, start=None, end=None):
        super(BitstreamExtractBitstream, self).__init__(lineno)
        self.name = name
        self.start = start
        self.end = end

        if self.start is None:
            self.start = 0


    def verify_node(self, context, event):
        # We need to verify :
        # 1) the variable exists and correspond to a bitstream
        self.test_type(self.name, Bitstream(), context, event)

        # 2) indexes value are integers and verify them
        for bound in [self.start, self.end]:
            if bound is not None:
                self.test_type(bound, Integer(1), context, event)
                bound.verify_node(context, event)


        if isinstance(self.start, ConstantExpression) and isinstance(self.end, ConstantExpression):
            start = self.start.value
            end = self.end.value

            if start >= end:
                context.add_error('The range of the extraction is not valid: [%d:%d]' % (start, end))
            if end - start >= 8 * 1500:
                context.add_error('The range of the extraction is not valid (too big): [%d:%d]' % (start, end))


    def get_type(self, context, event):
        """
        If we know statically the value of the start and the end, then it is an Integer
        Otherwise, it's a bitstream

        To be converted to an integer, we need to know statically the bounds
        Also, the bounds must be consistent :
            -   4 :  10 -> Integer
            -  10 : -10 -> Bitstream
            -     :  10 -> Integer
            - -10 :     -> Integer
        """
        if not self.is_statically_known(self.start) or not self.is_statically_known(self.end):
            return Bitstream()

        if self.is_constant(self.start) and self.is_constant(self.end):
            start_val, end_val = self.start.value, self.end.value
            start_sign, end_sign = self.get_sign(start_val), self.get_sign(end_val)

            if start_sign == end_sign:
                # start and end values have the same sign
                # example: 5 : 10 or -10 : -5
                return Integer(1)
            else:
                # different signs
                # example : 10 : -10
                return Bitstream()

        else:
            # There is at least one limit bound (min or max)
            # min is already modified into 0 so there is only max to deal with
            start_val = self.start.value
            if self.get_sign(start_val):
                return Bitstream()
            else:
                return Integer(1)


    def get_sign(self, number):
        return 1 if number >= 0 else -1

    def is_constant(self, bound):
        """ Determine whether or not a bound is statically known: a constant or the start/end of the bitstream """
        return isinstance(bound, ConstantExpression)

    def is_boundary(self, bound):
        return bound is None

    def is_statically_known(self, bound):
        return self.is_boundary(bound) or self.is_constant(bound)

    def get_expressions_of_type(self, exp_class):
        siblings = filter(lambda e: isinstance(e, Expression), [self.name, self.start, self.end])
        return self.check_expression_of_type(exp_class, siblings)


# ----- BINARY EXPRESSIONS -----

class BinaryOperator(Expression):
    """ Abstract class to represent a binary operator """
    def __init__(self, op_left, op_right, lineno):
        super(BinaryOperator, self).__init__(lineno)
        self.op_left = op_left
        self.op_right = op_right

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.op_left, self.op_right])



class GenericBinaryOperator(BinaryOperator):
    """ Abstract class to represent several operator of the same type """
    def __init__(self, op_left, op, op_right, lineno):
        super(GenericBinaryOperator, self).__init__(op_left, op_right, lineno)

        valid_types = [Integer(1), Time(), AnyEnumType(), NetAddr()]
        self.expected_types = (valid_types, valid_types)
        self.operator = op

    def verify_node(self, context, event):
        # Verify both operands
        self.op_left.verify_node(context, event)
        self.op_right.verify_node(context, event)

        # Verify that both operands are of expected types
        self.test_types(self.op_left, self.expected_types[0], context, event)
        self.test_types(self.op_right, self.expected_types[1], context, event)

    def get_type(self, context, event):
        return Integer(1)


class ArithmeticOperator(GenericBinaryOperator):
    """ Represent a binary operator: +-/* """
    pass


class ConcatBitstreamOperator(BinaryOperator):
    """ Represent a concat binary operator for bitstreams """
    pass


class ComparisonOperator(GenericBinaryOperator):
    """ Represent a comparison operator : >, >=, ==, !=, <=, < """
    pass


class LogicalOperator(GenericBinaryOperator):
    """ Represent a logical operator: and, or, etc. """
    pass


class InOperator(BinaryOperator):
    """ Return whether or not a needle is in an haystack
    There are several valid expression for this operator :
        - an IP address in an IP network
        - a value in a list of constant / enum
    """

    def __init__(self, op_left, op_right, lineno):
        super(InOperator, self).__init__(op_left, op_right, lineno)

    def verify_node(self, context, event):
        # Verify both operands
        self.op_left.verify_node(context, event)
        self.op_right.verify_node(context, event)

        # Verify that types are correct
        self.test_types(self.op_left, [NetAddr(), Integer(1)], context, event)
        self.test_types(self.op_right, [NetAddr(), AnyEnumType()], context, event)


    def get_type(self, context, event):
        return Integer(1)


# ----- NETWORK RELATED EXPRESSIONS -----

class NetAddr4(Expression):
    def __init__(self, raw_address, raw_mask, lineno):
        super(NetAddr4, self).__init__(lineno)


        self.address = raw_address.value
        self.set_metadata('address', self.get_metadata_from(raw_address))

        if raw_mask is not None:
            self.mask = raw_mask.value
            self.set_metadata('mask', self.get_metadata_from(raw_mask))
        else:
            self.mask = 32


    def verify_node(self, context, event):
        # Verify that the address is correct
        if not self.is_valid_address():
            context.add_error('IPv4 address "%s" is not valid' % '.'.join([str(b) for b in self.address]), self.lineno)

        # Verify that the mask is correct
        if not self.is_valid_mask():
            context.add_error('IPv4 mask "%d" is not valid' %  self.mask, self.lineno)

    def is_valid_address(self):
        return sum([(b <= 255 and b >= 0) for b in self.address]) == 4

    def is_valid_mask(self):
        return self.mask >= 0 and self.mask <= 32

    def get_type(self, context, event):
        return NetAddr()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class)

class NetworkFunction(Expression):
    def __init__(self, address, mask, lineno):
        super(NetworkFunction, self).__init__(lineno)
        self.address = address
        self.mask = mask


class ToNet4(NetworkFunction):
    def get_type(self, context, event):
        # First, check if addr/mask have correct types
        self.test_type(self.address, Integer(1), context, event)
        self.test_type(self.mask, Integer(1), context, event)
        return NetAddr()

    def verify_node(self, context, event):
        self.address.verify_node(context, event)
        self.mask.verify_node(context, event)

        self.test_type(self.address, Integer(0), context, event)
        self.test_type(self.mask, Integer(0), context, event)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.address, self.mask])


class ToNet6(NetworkFunction):
    pass


# ----- SET OPERATIONS -----

class ExistsOperation(Expression):
    def __init__(self, table_name, values, lineno):
        self.table_name = table_name
        self.values = values
        self.set_lineno(lineno)

    def verify_node(self, context, event):
        # Verify that table_name represents a table
        if self.table_name not in context.tables:
            context.add_error('Table "%s" does not exist' % self.table_name, self.lineno)

        # Verify each value
        for value_name, value_exp in self.values:
            value_exp.verify_node(context, event)

            if value_name not in context.tables[self.table_name].fields_type:
                context.add_error('Table "%s" does not have a field entitled "%s"' % (self.table_name, value_name), self.lineno)


    def get_type(self, context, event):
        return Integer(1)


class ForOperation(Expression):
    def __init__(self, raw_for_type, raw_var_name, raw_table_name, exp, lineno):
        super(ForOperation, self).__init__(lineno)
        self.for_type = raw_for_type.value
        self.var_name = raw_var_name.value
        self.table_name = raw_table_name.value
        self.exp = exp
        self.set_lineno(lineno)

        # Update metadata
        self.set_metadata('for_type', self.get_metadata_from(raw_for_type))
        self.set_metadata('var_name', self.get_metadata_from(raw_var_name))
        self.set_metadata('table_name', self.get_metadata_from(raw_table_name))

    def verify_node(self, context, event):
        # Verify that table_name represents a table
        if self.table_name not in context.tables:
            context.add_error('Table "%s" does not exist' % self.table_name, self.lineno)

        event.add_set_identifier(self.var_name, self.table_name)

        # Verify expression
        self.exp.verify_node(context, event)





class MatchExpression(Expression):
    def __init__(self, haystack, raw_needle, raw_groups, lineno):
        super(MatchExpression, self).__init__(lineno)
        self.haystack = haystack
        self.needle = raw_needle.value
        self.groups = [elt.value for elt in raw_groups]

        # Update metadata
        self.set_metadata('needle', self.get_metadata_from(raw_needle))
        self.set_metadata('groups', [
                self.get_metadata_from(elt) for elt in raw_groups
            ])

    def verify_node(self, context, event):
        # Verify haystack type
        self.test_type(self.haystack, Bitstream(), context, event)
        self.haystack.verify_node(context, event)

        # Verify that the pattern is correct
        try:
            re.compile(self.needle)
        except re.error:
            context.add_error('Regex is invalid "%s"' % (self.needle), self.lineno)


        # Manage new variables from extracted groups
        for idx, group in enumerate(self.groups):
            if is_declared_identifier(group, context, event):
                context.add_error('Identifier "%s" has been already declared' % group, self.lineno)
            else:
                event.add_match_identifier(group, MatchGroupExpression(group, self, idx, self.lineno))

        # Verify that the needle is a correct regexp
        #try:
            #re.compile(self.needle)
        #except re.error:
            #context.add_error('Invalid regexp "%s"' % self.needle)


    def get_type(self, context, event):
        return Integer(1)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.haystack])


class MatchGroupExpression(Expression):
    def __init__(self, match, var_name, index, lineno):
        super(MatchGroupExpression, self).__init__(lineno)
        self.var_name = var_name
        self.match = match
        self.index = index

    def verify_context(self, context, event):
        pass

    def get_type(self, context, event):
        return Bitstream()





# ----- UTIL FUNCTIONS -----

class UtilFunction(Expression):
    def __init__(self, value, lineno):
        super(UtilFunction, self).__init__(lineno)
        self.value = value

    def get_type(self, context, event):
        return Integer(1)

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.value])

class Len(UtilFunction):
    def verify_node(self, context, event):
        self.value.verify_node(context, event)
        self.test_type(self.value, Bitstream(), context, event)


class AsBig(UtilFunction):
    pass

class AsLittle(UtilFunction):
    pass


# ----- CONVERT FUNCTIONS -----
class ToInteger(Expression):
    def __init__(self, size, value, lineno):
        super(ToInteger, self).__init__(lineno)
        self.size = size
        self.value = value

    def get_type(self, context, event):
        return Integer(self.size)

class ToEnum(Expression):
    def __init__(self, enum_name, value, lineno):
        super(ToEnum, self).__init__(lineno)
        self.enum_name = enum_name
        self.value = value

    def verify_node(self, context, event):
        if self.enum_name not in context.enums:
            context.add_error('Unknown enum %s' % (self.enum_name), self.lineno)

        self.value.verify_node(context, event)

    def get_type(self, context, event):
        return EnumType(self.enum_name)




# ----- TABLE RELATED EXPRESSIONS -----

class TableInsert(Expression):
    """
        Represents a data insertion in a table
    """
    def __init__(self, raw_name, assignments, lineno):
        super(TableInsert, self).__init__(lineno)
        self.table = raw_name.value
        self.assignments = assignments

        # Update metadata
        self.set_metadata('table', self.get_metadata_from(raw_name))

    def verify_node(self, context, event):
        # We must check if :
        # - check that the table exists
        # - assignements use correct identfiers
        # - type of table field matches expression's type
        if self.table not in context.tables:
            context.add_error('Table "%s" does not exist' % (self.table), self.lineno)

        table = context.tables[self.table]

        for identifier, value in self.assignments:
            # identifier is an instance of Identifier, retrieve its name
            identifier = identifier.name
            if identifier not in table.fields_type:
                context.add_error('Table "%s" does not have a field entitled "%s"' % (self.table, identifier), self.lineno)
            else:
                self.test_type(value, table.fields_type[identifier], context, event)

        # TODO verify every field has been assigned to a value ?


    def get_type(self, context, event):
        return Undefined()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [elt[1] for elt in self.assignments])


class TableUpdate(Expression):
    """
        Represents a data update in a table
    """
    def __init__(self, record, value, lineno):
        super(TableUpdate, self).__init__(lineno)
        self.record = record
        self.table_name = record.container
        self.field_name = record.name
        self.value = value

    def verify_node(self, context, event):
        record_type = self.value .get_type(context, event)

        if not isinstance(record_type, Undefined):
            self.test_type(self.value, record_type, context, event)

    def get_type(self, context, event):
        return Undefined()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, [self.value])


# ----- EVENT RELATED EXPRESSIONS -----

class RaiseEvent(Expression):
    def __init__(self, raw_name, args, delay, lineno):
        super(RaiseEvent, self).__init__(lineno)
        self.name = raw_name.value
        self.args = args
        self.delay = delay

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))

    def verify_node(self, context, event):

        # Verify that raised event exists
        if self.name not in context.events:
            context.add_error('Unknown event "%s" raised by "%s"' % (self.name, event.name), self.lineno)
        else:
            # Verify each argument
            for arg in self.args:
                arg.verify_node(context, event)

            # Args type
            args_types = [arg.get_type(context, event) for arg in self.args]
            args_name = context.events_args_name[self.name]
            expected_types = context.events_type[self.name]

            if not event_mod.Event.correct_types_number(args_types, expected_types):
                context.add_error('Incorrect number of args to raise event "%s" in event "%s" (%d given, %d expected)' \
                        % (self.name, event.name, len(args_types), len(expected_types)), self.lineno)

            for idx in event_mod.Event.correct_types(args_types, expected_types):
                context.add_error('Incorrect type (%s) for %dth argument (%s) for event "%s" (expected %s)' \
                        % (args_types[idx].__class__.__name__, idx + 1, args_name[idx],
                            self.name, expected_types[idx].__class__.__name__), self.lineno)

    def get_type(self, context, event):
        return Undefined()


    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, self.args)


class AlertExpression(Expression):
    def __init__(self, raw_category, raw_message, params, lineno):
        super(AlertExpression, self).__init__(lineno)
        self.category = raw_category.value
        self.message = raw_message.value
        self.params = params

        # Update metadata
        self.set_metadata('category', self.get_metadata_from(raw_category))
        self.set_metadata('message', self.get_metadata_from(raw_message))


    def verify_node(self, context, event):
        for param in self.params:
            param.verify_node(context, event)


    def get_type(self, context, event):
        return Undefined()

    def get_expressions_of_type(self, exp_class):
        return self.check_expression_of_type(exp_class, self.params)


class AlertParamExpression(Variable):
    def __init__(self, name, value, lineno):
        super(AlertParamExpression, self).__init__(name, value, lineno)

    def verify_node(self, context, event):
        # First, we need to verify that the name is correct
        if self.name not in ['start_time', 'end_time', 'description', 'severity', 'confidence', 
                'completion', 'reference']:
            context.add_error('Incorrect alert parameter: %s' % self.name, self.lineno)

        # Then, we verify the value
        self.value.verify_node(context, event)



# ---- SHORTCUT FUNCTIONS
def is_declared_identifier(name, context, event):
    return context.exists_identifier(name) or event.exists_identifier(name)


def get_identifier_type(name, context, event):
    if context.exists_identifier(name):
        return context.get_identifier_type(name)

    if event.exists_identifier(name):
        return event.get_identifier_type(context, name)

    return Undefined()
