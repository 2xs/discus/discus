'''
File: context.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: While parsing Discus language, we need to store statements, declarations and so on.
    We use a context to do so
'''

import sys
from collections import defaultdict
import cPickle as pickle
from event import Event
from type import Undefined

context = None

class Context:

    def __init__(self, filename):
        self.filename = filename
        self.tables           = {}
        self.enums            = {}
        self.variables        = []
        self.events           = defaultdict(list)
        self.events_label     = defaultdict(list)
        self.events_type      = {}
        self.events_args_name = {}
        self.errors           = defaultdict(list)
        # Following identifiers are added when declared
        self.var_identifiers = {}


    def add_event_label(self, event, category, message):
        self.events_label[event].append((category, message))

    def add_error(self, message, lineno=0):
        if lineno != 0: message = "line %d: %s" % (lineno, message)
        self.errors[lineno].append(message)

    def nb_errors(self):
        return sum([len(self.errors[key]) for key in self.errors])

    def print_errors(self):
        sys.stderr.write('Discus Script - %d error(s) - compilation aborted\n-----\n' % self.nb_errors())
        for lineno in sorted(self.errors):
            for error in self.errors[lineno]:
                sys.stderr.write(error + '\n')

    def add_variable(self, variable):
        self.variables.append(variable)

    def add_var_identifier(self, name, value):
        self.var_identifiers[name] = value


    def add_event(self, event):
        """ Add an event into the dict according to its name """
        if event.name not in self.events_type:
            # New event with this name
            # We fill the event type
            self.events_type[event.name] = [arg.type for arg in event.args]
            self.events_args_name[event.name] = [arg.name for arg in event.args]

        else:
            # We need to check whether or not this event type is correct
            given_types = [arg.type for arg in event.args]
            expected_types = self.events_type[event.name]

            if not Event.correct_types_number(given_types, expected_types):
                self.add_error('incorrect number of args for event "%s" (%d given, %d expected)' \
                        % (event.name, len(given_types), len(expected_types)), event.lineno)

            for idx in Event.correct_types(given_types, expected_types):
                context.add_error('incorrect type (%s) for %dth argument for event "%s" (expected %s)' \
                        % (given_types[idx].__class__.__name__, idx + 1, 
                            event.name, expected_types[idx].__class__.__name__), event.lineno)


        self.events[event.name].append(event)

    def add_enum(self, enum):
        self.enums[enum.name] = enum

    def add_table(self, table):
        self.tables[table.name] = table

    def set_table_purge_stmt(self, table, entry_var, expression, lineno):
        if self.correct_table_stmt('purge', table, entry_var, expression, lineno):
            table = self.tables[table]
            table.set_purge_stmt(entry_var, expression)

    def set_table_remove_stmt(self, table, entry_var, expression, lineno):
        if self.correct_table_stmt('remove', table, entry_var, expression, lineno):
            table = self.tables[table]
            table.set_remove_stmt(entry_var, expression)


    def correct_table_stmt(self, stmt_type, table_name, entry_var, expression, lineno):
        # First verify the table exists
        if table_name not in self.tables:
            self.add_error('Table "%s" does not exists' % table_name, lineno)
        elif self.exists_identifier(entry_var):
            self.add_error('Identifier "%s" has been already declared' % entry_var, lineno)
        else:
            return True
        return False





    def verify_context(self):
        # Verifying tables and enums
        for elements in [self.tables.values(), self.enums.values()]:
            for element in elements:
                element.verify_node(self)

        # Verify global variables
        for variable in self.variables:
            variable.verify_node(self, Event(None, [], 0))

        # Verifying events
        for event_name, event_rules in self.events.items():
            for event_rule in event_rules:
                event_rule.verify_node(self)


    def trigger_actions(self, event):
        """ Return whether or not this event triggers an action
            An action is : an alert, a data insertion/update
        """
        return True in [rule.trigger_actions() for rule in self.events[event]]


    def exists_identifier(self, name):
        """
            Return whether or not an identifier is reserved.
            A reserved identifier is : enums name, tables, name, event_names, variable_names
        """
        return name in self.tables or name in self.enums or name in self.events or name in self.var_identifiers


    def get_identifier_type(self, name):
        """
            Return the type of a (global variable)
        """
        if name in self.var_identifiers:
            return self.var_identifiers[name].get_type(self, Event(None, [], 0))

        return Undefined()


    def get_expressions_of_type(self, exp_class):
        exps = []

        # Looking for such expression in events
        for event_list in self.events.values():
            for event in event_list:
                if isinstance(event, str):
                    import ipdb; ipdb.set_trace()
                exps.extend(event.get_expressions_of_type(exp_class))

        # Looking for such expression in tables
        for table in self.tables.values():
            exps.extend(table.get_expressions_of_type(exp_class))

        return exps




def load_context(input):
    """ Load a context from a pickled file """
    return pickle.load(input)
