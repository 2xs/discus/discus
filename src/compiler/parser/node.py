'''
File: node.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Every element of Discus Script (expression, statement, declaration) are nodes
    This file provide the abstract class to represent a node
'''

class DiscusNode(object):

    def __init__(self):
        self.lineno = 0
        self.metadata = {}

    def set_lineno(self, lineno):
        self.lineno = lineno

    def set_metadata(self, name, value):
        self.metadata[name] = value

    def set_metadata_from(self, name, source):
        self.metadata[name] = self.get_metadata_from(source)

    def get_metadata_from(self, raw_data):
        """
        Filter raw resources about a token:
            - start offset
            - lineno
        """
        if raw_data is None:
            return None
        return {
            'start_offset': raw_data.lexpos,
            'end_offset': raw_data.lexpos + len(str(raw_data.value)),
            'lineno': raw_data.lineno,
        }

    def get_metadata(self, name):
        return self.metadata[name]



    def verify_node(self, global_context):
        """
            Abstract method that every inherited classes must override
            This method verify that a node is valid
        """
        print '%s.verify_node might not be implanted yet' % self.__class__.__name__
        return False

    def convert_to_engine(self):
        pass

    def error_message(self, message):
        """ Returns a formated message """
        return "line %d: %s" % (self.lineno, message)
