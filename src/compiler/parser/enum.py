'''
File: enum.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Discus Compiler
    Represent an enum
'''

import node

class Enum(node.DiscusNode):
    """ An enum is a sequence of ordered key,value """

    def __init__(self, raw_name, raw_fields, context, lineno):
        # Initializing super class
        super(Enum, self).__init__()
        self.set_lineno(lineno)

        # Name of the enum
        self.name = raw_name.value

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))
        self.set_metadata('fields',
            [(self.get_metadata_from(key), self.get_metadata_from(val)) for key, val in raw_fields]
        )

        # Init the list of key, value
        # Value can be set or not
        current_value = -1

        self.fields_list = []
        self.fields_dict = {}


        for raw_key, raw_value in raw_fields:
            key = raw_key.value

            if raw_value is None:
                current_value += 1
                value = current_value
            else:
                value = raw_value.value.value

            if key in self.fields_dict:
                context.add_error(self.error_message("Enum '%s' has several fields entitled '%s'" % (self.name, key)))

            self.fields_list.append((key, value))
            self.fields_dict[key] = value



    def keys(self):
        """ Return the keys of this enum """
        return self.fields_dict.keys()

    def get(self, key):
        if key in self.fields_dict:
            return self.fields_dict[key]
        return None


    def verify_node(self, context):
        return True
