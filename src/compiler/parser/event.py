
'''
File: event.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Description of an event
'''

from collections import defaultdict
from expression import Expression, TableInsert, TableUpdate, RaiseEvent, AlertExpression
import node
import type


class Event(node.DiscusNode):

    def __init__(self, raw_name, args, lineno):
        super(Event, self).__init__()
        self.set_lineno(lineno)
        self.name = raw_name.value if raw_name is not None else ""
        self.args = args
        self.variables = []
        self.for_stmt = None
        self.where = None
        self.all_statements = []
        self.ifnone = []
        self.inserts = []
        self.updates = []
        self.raised_events = []
        self.alerts = []
        self.labels = defaultdict(list)
        # Following identifiers are added when declared
        self.set_identifiers = {}
        self.var_identifiers = {}
        self.match_identifiers = {}

        # Update metadata
        self.set_metadata('name', self.get_metadata_from(raw_name))


    def add_label(self, category, message):
        self.labels[category].append(message)

    def add_variables(self, variables):
        self.variables.extend(variables)


    def set_for(self, for_stmt):
        self.for_stmt = for_stmt


    def set_ifnone(self, ifnone):
        self.ifnone = ifnone

    def add_statements(self, statements):
        """ Add statements that can be table insertion/update, raised events and so on """
        class_stmts = {
            TableInsert : self.inserts,
            TableUpdate : self.updates,
            RaiseEvent : self.raised_events,
            AlertExpression : self.alerts
        }

        self.all_statements.extend(statements)

        for stmt in statements:
            for stmt_class, stmt_list in class_stmts.items():
                if isinstance(stmt, stmt_class):
                    stmt_list.append(stmt)

    def set_where(self, where):
        self.where = where

    def verify_node(self, context):
        local_context = {}

        # Arguments
        declared_args = []
        for arg in self.args:
            if arg.name in declared_args:
                context.add_error('Identifier "%s" already used in the arguments' % arg.name, arg.lineno)
            else:
                declared_args.append(arg.name)

            if context.exists_identifier(arg.name):
                context.add_error('Identifier "%s" has been already declared' % arg.name, self.lineno)



        # Need to verify every expression of each statement
        for var in self.variables:
            var.verify_node(context, self)


        for stmt in ['where', 'for_stmt']:
            stmt_attr = getattr(self, stmt)
            if stmt_attr is not None:
                stmt_attr.verify_node(context, self)


        for stmts in [self.inserts, self.updates, self.raised_events, self.alerts, self.ifnone]:
            for stmt in stmts:
                stmt.verify_node(context, self)


    def trigger_actions(self):
        """ Return whether or not this event triggers an action
            An action is : an alert, a data insertion/update
        """
        return len(self.inserts) or len(self.updates) or len(self.alerts)


    def exists_identifier(self, name):
        """
            Verify if a variable exists
            A variable could be an argument, a local variable or
            extracted arguments (from match expression)
        """
        # Arg identifier
        if name in [arg.name for arg in self.args]:
            return True

        # Local variable
        if name in self.var_identifiers:
            return True

        # Set identifier (exists/forall expression)
        if name in self.set_identifiers:
            return True

        # Match statement
        if name in self.match_identifiers:
            return True

        return False

    def get_identifier_type(self, context, name):
        """
            Return the type of a variable (we suppose it exists)
        """
        # Arg identifier
        for arg in self.args:
            if name == arg.name:
                return arg.type

        # Local variable
        if name in self.var_identifiers:
            return self.var_identifiers[name].get_type(context, self)

        # Match variable
        if name in self.match_identifiers:
            return self.match_identifiers[name].get_type(context, self)

        return type.Undefined()

    def add_set_identifier(self, name, table_name):
        self.set_identifiers[name] = table_name

    def add_var_identifier(self, name, value):
        self.var_identifiers[name] = value

    def add_match_identifier(self, name, group_expression):
        self.match_identifiers[name] = group_expression

    def get_args_type(self):
        """ Return the type of args """
        return [arg.type for arg in self.args]


    def get_expressions_of_type(self, exp_class):
        exps = []

        if self.where is not None:
            exps.extend(self.where.get_expressions_of_type(exp_class))

        for stmt_list in ['all_statements', 'ifnone', 'variables']:
            for stmt in getattr(self, stmt_list):
                exps.extend(stmt.get_expressions_of_type(exp_class))

        return exps




    @staticmethod
    def correct_types_number(given_types, expected_types):
        return len(given_types) == len(expected_types)

    @staticmethod
    def correct_types(given_types, expected_types):
        incorrect = []

        for idx, types in enumerate(zip(given_types, expected_types)):
            cur_type, exp_type = types
            if not isinstance(cur_type, exp_type.__class__):
                incorrect.append(idx)

        return incorrect
