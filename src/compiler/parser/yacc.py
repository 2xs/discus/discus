'''
File: yacc.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: This file contains parser data about Discus language
'''

import ply.yacc as yacc
from lex import tokens

from context import Context, context
from event import Event
from type import *
from expression import *
from enum import *
from table import Table
from field import Field


def p_error(p):
    if p is None:
        return
    message = 'line %d: unexpected symbol %s with value "%s"' % (p.lineno, p.type, p.value)
    context.add_error(message)


def parse_file(filename):
    global context
    context = Context(filename)

    try:
        with open(filename) as f:
            yacc.parse(f.read(), tracking=True)
    except EOFError:
        context.add_error("Unexpected EOF while parsing")

    return context




# ------------- PRECEDENCE ----------------
precedence = (
    ('nonassoc', 'USET', 'IN'),
    ('nonassoc', 'OP_EQ', 'OP_NE', 'OP_GT', 'OP_LT', 'OP_GE', 'OP_LE', 'MATCH'),
    ('left', 'PLUS', 'MINUS', 'CONCAT'),
    ('left', 'TIMES', 'DIVIDE'),
    ('left', 'AND', 'OR'),
    ('right', 'UNOT', 'UMINUS')
)


# ------------- START ---------------------
def p_start(p):
    " start : statement_list "
    p[0] = context

def p_statement_list(p):
    ''' statement_list :
            | statement statement_list
    '''
    pass

def p_statement(p):
    " statement : instruction ';' "
    pass

def p_instruction_table_decl(p):
    ''' instruction : table_declaration
    '''
    context.add_table(p[1])


def p_instruction_enum_decl(p):
    " instruction : enum_declaration "
    context.add_enum(p[1])


def p_instruction_event_rule(p):
    " instruction : event_rule "
    context.add_event(p[1])

def p_instruction_variable(p):
    " instruction : global_var "
    context.add_variable(p[1])

def p_instruction_table_remove(p):
    " instruction : table_remove_rule "
    pass

def p_instruction_table_purge(p):
    " instruction : table_purge_rule "
    pass

# ------------- GLOBAL VAR ---------------------
def p_global_variable(p):
    " global_var : LET STRING '=' constant "
    p[0] = GlobalVariable(p.slice[2], p[4], p.lineno(1))

    # Update metadata
    p[0].set_metadata_from('let', p.slice[1])

# ------------- TABLE ---------------------
def p_table_declaration(p):
    '''
        table_declaration : TABLE STRING '{' field_declaration_list '}'
    '''
    p[0] = Table(p.slice[2], p[4], context, p.lineno(1))

    # Update metadata
    p[0].set_metadata_from('table', p.slice[1])


def p_field_declaration_list(p):
    ''' field_declaration_list : field_declaration
            | field_declaration_list field_declaration
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = p[1]
        p[0].extend(p[2])


def p_field_declaration(p):
    " field_declaration : type_specifier field_names ';' "
    p[0] = [Field(name, p[1]) for name in p[2]]


def p_field_names(p):
    ''' field_names : STRING
            | STRING ',' field_names
    '''
    if len(p) == 2:
        p[0] = [p.slice[1]]
    else:
        p[0] = p[3]
        p[0].insert(0, p.slice[1])

def p_table_remove_rule(p):
    " table_remove_rule : REMOVE STRING FROM STRING WHEN expression "
    context.set_table_remove_stmt(p[4], p[2], p[6], p.lineno(1))


def p_table_purge_rule(p):
    " table_purge_rule : ON PURGE STRING SELECT STRING WHERE expression "
    context.set_table_purge_stmt(p[3], p[5], p[7], p.lineno(1))


# ------------- ENUM ---------------------
def p_enum_declaration(p):
    " enum_declaration : ENUM_TYPE STRING '{' enum_field_list '}' "
    p[0] = Enum(p.slice[2], p[4], context, p.lineno(1))

    # Update metadata
    p[0].set_metadata_from('enum', p.slice[1])

def p_enum_field_list(p):
    ''' enum_field_list : enum_field
            | enum_field ',' enum_field_list
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[3]
        p[0].insert(0, p[1])


def p_enum_field(p):
    ''' enum_field : STRING
            | STRING '=' integer_constant
    '''
    if len(p) == 2:
        p[0] = (p.slice[1], None)
    else:
        p[0] = (p.slice[1], p.slice[3])


# ------------- EVENT ---------------------
def p_event_rule(p):
    " event_rule : ON event_name let_list where_stmt for_stmt event_stmt_list ifnone_stmt"
    name, args = p[2]

    p[0] = Event(name, args, p.lineno(1))

    p[0].add_variables(p[3])
    p[0].set_where(p[4][0])
    p[0].set_for(p[5])
    p[0].add_statements(p[6])
    p[0].set_ifnone(p[7][0])

    # Update metadata
    p[0].set_metadata_from('on', p.slice[1])
    p[0].set_metadata_from('where', p[4][1])
    p[0].set_metadata_from('ifnone', p[7][1])



def p_event_name(p):
    " event_name : STRING '(' proto_argument_list ')' "
    p[0] = (p.slice[1], p[3])


def p_let_list(p):
    ''' let_list :
            | let_stmt let_list
    '''
    if len(p) == 1:
        p[0] = []
    else:
        p[0] = p[2]
        p[0].append(p[1])

def p_let_stmt(p):
    " let_stmt : LET STRING '=' expression "
    p[0] = Variable(p.slice[2], p[4], p.lineno(1))

    # Update metadata
    p[0].set_metadata_from('let', p.slice[1])


def p_where_stmt(p):
    ''' where_stmt :
            | WHERE expression
    '''
    if len(p) == 1:
        p[0] = (None, None)
    else:
        p[0] = (p[2], p.slice[1])


def p_for_stmt(p):
    ''' for_stmt :
            | FOR ALL STRING IN STRING WITH expression
            | FOR FIRST STRING IN STRING WITH expression
    '''
    if len(p) == 1:
        p[0] = None
    else:
        p[0] = ForOperation(p.slice[2], p.slice[3], p.slice[5], p[7], p.lineno(1))

        # Update metadata
        p[0].set_metadata_from('for', p.slice[1])
        p[0].set_metadata_from('in', p.slice[4])
        p[0].set_metadata_from('with', p.slice[6])


def p_event_stmt_list(p):
    ''' event_stmt_list :
            | event_stmt_list event_stmt
    '''
    if len(p) == 1:
        p[0] = []
    else:
        p[0] = p[1]
        p[0].append(p[2])


def p_ifnone_stmt(p):
    ''' ifnone_stmt :
            | IFNONE event_stmt_list
    '''
    if len(p) == 1:
        p[0] = ([], None)
    else:
        p[0] = (p[2], p.slice[1])


# ---- INSERT
def p_insert_expression(p):
    " event_stmt : INSERT INTO STRING '{' assignment_list '}' "
    p[0] = TableInsert(p.slice[3], p[5], p.lineno(1))

    # update metadata
    p[0].set_metadata_from('insert', p.slice[1])
    p[0].set_metadata_from('into', p.slice[2])

def p_assignment_list(p):
    ''' assignment_list : assignment_expression
            | assignment_expression assignment_list
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[2]
        p[0].insert(0, p[1])

def p_assignment_expression(p):
    " assignment_expression : variable_name '=' expression ';' "
    p[0] = (p[1], p[3])

# ---- UPDATE
def p_update_expression(p):
    " event_stmt : UPDATE field_access '=' expression"
    p[0] = TableUpdate(p[2], p[4], p.lineno(1))

    # update metadata
    p[0].set_metadata_from('update', p.slice[1])

# ---- ALERT
def p_alert_expression(p):
    ''' event_stmt : ALERT '(' QUOTED_STRING ',' QUOTED_STRING ')'
            | ALERT '(' QUOTED_STRING ',' QUOTED_STRING ',' alert_param_list ')'
    '''
    if len(p) == 7:
        p[0] = AlertExpression(p.slice[3], p.slice[5], [], p.lineno(1))
    else:
        p[0] = AlertExpression(p.slice[3], p[5], p.slice[7], p.lineno(1))

    # update metadata
    p[0].set_metadata_from('alert', p.slice[1])


def p_alert_param_list(p):
    ''' alert_param_list : alert_param
            | alert_param_list ',' alert_param
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1]
        p[0].append(p[2])

def p_alert_param(p):
    " alert_param : STRING '=' expression "
    p[0] = AlertParamExpression(p[1], p[3], p.lineno(1))


# ---- RAISE EVENT
def p_raised_event(p):
    " event_stmt : RAISE STRING '(' expression_list ')'"
    p[0] = RaiseEvent(p.slice[2], p[4], 0, p.lineno(1))

    # update metadata
    p[0].set_metadata_from('raise', p.slice[1])


def p_raised_event_delay(p):
    " event_stmt : RAISE STRING '(' expression_list ')' IN integer_constant "
    p[0] = RaiseEvent(p.slice[2], p[4], p[7], p.lineno(1))

    # update metadata
    p[0].set_metadata_from('raise', p.slice[1])


# ------------- EXPRESSION ---------------------
def p_expression(p):
    ''' expression : atomic_expression
                   | match_expression
                   | expression PLUS expression
                   | expression MINUS expression
                   | expression TIMES expression
                   | expression DIVIDE expression
                   | expression CONCAT expression
                   | expression OP_EQ expression
                   | expression OP_NE expression
                   | expression OP_GE expression
                   | expression OP_GT expression
                   | expression OP_LE expression
                   | expression OP_LT expression
                   | expression OR expression
                   | expression AND expression
                   | expression IN expression
    '''
    if len(p) == 2:
        p[0] = p[1]

    elif len(p) == 4:
        if p[2] in '+-*/':
            p[0] = ArithmeticOperator(p[1], p[2], p[3], p.lineno(2))

        elif p[2] == '++':
            p[0] = ConcatBitstreamOperator(p[1], p[3], p.lineno(2))

        elif p[2] in ['==', '!=', '<', '<=', '>=', '>']:
            p[0] = ComparisonOperator(p[1], p[2], p[3], p.lineno(2))

        elif p[2] in ['or', 'and']:
            p[0] = LogicalOperator(p[1], p[2], p[3], p.lineno(2))

        elif p[2] == 'in':
            p[0] = InOperator(p[1], p[3], p.lineno(2))


def p_atomic_expression(p):
    ''' atomic_expression : variable
            | bitstream_manipulation
            | exists_expression
            | util_functions
            | network_functions
            | convert_functions
    '''
    p[0] = p[1]

def p_atomic_expression_constant(p):
    " atomic_expression : constant "
    p[0] = p[1]


def p_atomic_expression_parentheses(p):
    " atomic_expression : '(' expression ')' "
    p[0] = p[2]


def p_atomic_expression_not(p):
    " atomic_expression : NOT expression %prec UNOT "
    p[0] = NotExpression(p[2], p.lineno(1))


def p_atomic_expression_minus(p):
    " atomic_expression : MINUS expression %prec UMINUS"
    if isinstance(p[2], ConstantExpression):
        val = - p[2].value
        if isinstance(p[2].type, Integer):
            p[0] = ConstantExpression(Integer(get_binary_length(val)), val, p.slice[2],  p.lineno(1))

        if isinstance(p[2].type, Float):
            p[0] = ConstantExpression(Float(), val, p.slice[2], p.lineno(1))
    else:
        p[0] = MinusExpression(p[2], p.lineno(1))


def p_bitstream_manipulation_extract_bitstream(p):
    ''' bitstream_manipulation : variable '[' expression ':' expression ']'
            | variable '[' ':' expression ']'
            | variable '[' expression ':' ']'
    '''
    if len(p) == 7:
        # Both start and end
        p[0] = BitstreamExtractBitstream(p[1], p.lineno(2), start=p[3], end=p[5])
    elif len(p) == 6:
        if p[3] == ':':
            # Only the end
            p[0] = BitstreamExtractBitstream(p[1], p.lineno(2), end=p[4])
        else:
            # Only the start
            p[0] = BitstreamExtractBitstream(p[1], p.lineno(2), start=p[3])


def p_bitstream_manipulation_extract_bit(p):
    " bitstream_manipulation : variable '[' expression ']' "
    p[0] = BitstreamExtractBit(p[1], p[3], p.lineno(2))

def p_variable(p):
    ''' variable : variable_name
            | field_access
    '''
    p[0] = p[1]

def p_variable_name(p):
    ''' variable_name : STRING '''
    p[0] = IdentifierExpression(p.slice[1], p.lineno(1))

def p_field_access(p):
    " field_access : STRING DOT STRING "
    p[0] = RecordAccess(p.slice[1], p.slice[3], p.lineno(1))

# ------------- MATCH EXPRESSION ---------------------
def p_match_expression(p):
    ''' match_expression : expression MATCH QUOTED_STRING
            | expression MATCH QUOTED_STRING AS '(' arg_list ')'
    '''
    groups = [] if len(p) == 4 else p[6]
    p[0] = MatchExpression(p[1], p.slice[3], groups, p.lineno(2))

    # Update metadata
    p[0].set_metadata_from('match', p.slice[2])


def p_arg_list(p):
    ''' arg_list : STRING
            | arg_list ',' STRING
    '''
    if len(p) == 2:
        p[0] = [p.slice[1]]
    else:
        p[0] = p[1]
        p[0].append(p.slice[3])


# ------------- SET OPERATIONS ---------------------
def p_exists_operation(p):
    " exists_expression : EXISTS IN STRING '{' exists_value_list '}' %prec USET"
    p[0] = ExistsOperation(p[3], p[5], p.lineno(1))


def p_exists_value_list(p):
    ''' exists_value_list : exists_value
            | exists_value_list exists_value
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1]
        p[0].append(p[2])


def p_exists_value(p):
    " exists_value : STRING OP_EQ expression ';'"
    p[0] = (p[1], p[3])



# ------------- UTILS FUNCTION EXPRESSION ---------------------
def p_utils_len(p):
    " util_functions : LEN '(' expression ')' "
    p[0] = Len(p[3], p.lineno(1))


def p_utils_asbig(p):
    " util_functions : ASBIG '(' expression ')' "
    p[0] = AsBig(p[3], p.lineno(1))


def p_utils_aslittle(p):
    " util_functions : ASLITTLE '(' expression ')' "
    p[0] = AsLittle(p[3], p.lineno(1))

# ------------- CONVERSION FUNCTION EXPRESSION -----------------
def p_convert_integer(p):
    " convert_functions : INTEGER_TYPE '(' expression ')' "
    p[0] = ToInteger(p[1], p[3], p.lineno(1))

def p_convert_enum(p):
    " convert_functions : ENUM_TYPE '(' STRING ',' expression ')' "
    p[0] = ToEnum(p[3], p[5], p.lineno(1))

# ------------- NETWORK FUNCTION EXPRESSION -----------------
def p_network_ipv4(p):
    " network_functions : NET4 '(' expression ',' expression ')'"
    p[0] = ToNet4(p[3], p[5], p.lineno(1))

def p_network_ipv6(p):
    " network_functions : NET6 '(' expression ',' expression ')'"
    p[0] = ToNet6(p[3], p[5], p.lineno(1))



# ------------- EXPRESSION LIST -----------------
def p_expression_list(p):
    ''' expression_list : %prec USET
            | expression %prec USET
            | expression_list ',' expression %prec USET
    '''
    if len(p) == 1:
        p[0] = []
    elif len(p) == 2:
        p[0] = [p[1]]
    elif len(p) == 4:
        p[0] = p[1]
        p[0].append(p[3])


# ------------- CONSTANTS ---------------------
def p_constant_true(p):
    " constant : TRUE "
    p[0] = ConstantExpression(Integer(1), 1, p.slice[1], p.lineno(1))

def p_constant_false(p):
    " constant : FALSE "
    p[0] = ConstantExpression(Integer(1), 0, p.slice[1], p.lineno(1))

def p_constant_integer(p):
    " constant : integer_constant "
    # Need to compute the integer binary length
    p[0] = p[1]

def p_constant_dec_integer(p):
    " integer_constant : INT_CST "
    p[0] = ConstantExpression(Integer(get_binary_length(p[1])), p[1], p.slice[1], p.lineno(1))

def p_constant_hex_integer(p):
    " integer_constant : HEXA_CST "
    int_val = int(p[1][2:], 16)
    p[0] = ConstantExpression(Integer(get_binary_length(int_val)), int_val, p.slice[1], p.lineno(1))

def p_constant_now(p):
    " constant : NOW "
    p[0] = ConstantExpression(Time(), 'now', p.slice[1], p.lineno(1))

def p_constant_float(p):
    " constant : FLOAT_CST "
    p[0] = ConstantExpression(Float(), p[1], p.slice[1], p.lineno(1))

def p_constant_ipv4(p):
    " constant : IPv4_CST "
    p[0] = NetAddr4(p.slice[1], None, p.lineno(1))

def p_constant_ipv4_network(p):
    " constant : IPv4_CST DIVIDE INT_CST "
    p[0] = NetAddr4(p.slice[1], p.slice[3], p.lineno(1))

def p_constant_list(p):
    " constant : '[' anonym_enum ']' "
    p[0] = AnonymEnumExpression(p[2], p.lineno(1))


def p_anonym_enum(p):
    ''' anonym_enum : integer_constant
            | anonym_enum ',' integer_constant
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[0] = p[1]
        p[0].append(p[3])


# ------------- ARGUMENTS ---------------------
def p_proto_argument_list(p):
    ''' proto_argument_list :
            | proto_argument
            | proto_argument_list ',' proto_argument
    '''
    if len(p) == 1:
        p[0] = []
    elif len(p) == 2:
        # One argument
        p[0] = [p[1]]
    else:
        # Several arguments
        p[0] = p[1]
        p[0].append(p[3])


def p_proto_argument(p):
    " proto_argument : type_specifier STRING"
    p[0] = Field(p.slice[2], p[1])

# ------------- TYPE ---------------------

def p_type_specifier_int(p):
    " type_specifier : INTEGER_TYPE "
    p[0] = Integer(p[1])


def p_type_specifier_float(p):
    " type_specifier : FLOAT_TYPE "
    p[0] = Float()


def p_type_specifier_time(p):
    " type_specifier : TIME_TYPE "
    p[0] = Time()


def p_type_specifier_enum(p):
    " type_specifier : ENUM_TYPE STRING "
    p[0] = EnumType(p[2])


def p_type_specifier_bitstream(p):
    " type_specifier : BITSTREAM_TYPE "
    p[0] = Bitstream()


def p_type_specifier_ipaddr(p):
    " type_specifier : IPADDR_TYPE "
    p[0] = NetAddr()


# Create parser
yacc.yacc(debug=0, write_tables=0)

