'''
File: graph.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Generate a graph of event node based on the input context
'''

import argparse
import sys
from collections import defaultdict

from parser import context

class Graph:

    def __init__(self):
        self.nodes = []
        self.successors = defaultdict(set)
        self.predecessors = defaultdict(set)

    def add_node(self, node):
        self.nodes.append(node)

    def add_edge(self, from_node, to_node):
        self.successors[from_node].add(to_node)
        self.predecessors[to_node].add(from_node)

    def get_predecessors(self, node):
        return self.predecessors[node]

    def get_successors(self, node):
        return self.successors[node]

    def get_ancestors(self, node):
        ancestors = set()
        for predecessor in self.get_predecessors(node):
            ancestors.add(predecessor)
            ancestors.update(self.get_ancestors(predecessor))
        return ancestors

    def get_nodes(self):
        return self.nodes

    def get_leaves(self):
        return [node for node in self.nodes if not len(self.successors[node])]

    def get_roots(self):
        return [node for node in self.nodes if not len(self.predecessors[node])]


def create_graph(context):
    """ Create a graph from a context """
    graph = Graph()

    # for every event, we look for rules containing a raise statement
    for event_name, rules in context.events.items():
        graph.add_node(event_name)
        for rule in rules:
            for raised_event in rule.raised_events:
                graph.add_edge(event_name, raised_event.name)

    return graph
