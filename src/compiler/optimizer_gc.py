"""
File: optimizer_gc.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Look for rules, expressions or events to be removed
"""


import argparse, sys
import cPickle as pickle
from parser.context import load_context
from graph.graph import create_graph

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("--output", "-o", help="output file to store the compiled file (default: stdout)")
    parser.add_argument("entry", nargs='+', help="Entry point(s) of the graph")
    args = parser.parse_args()


    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)


    # Look for unused rules, events


    output = sys.stdout if not args.output else open(args.output, 'wb')
    pickle.dump(ctx, output)
    output.close()

