"""
File: generator.py
Author: Damien Riquet
Email: d.riquet@gmail.com
Description: Generate C files from a Discus context
"""

from collections import defaultdict, namedtuple
from renderer import Renderer
from parser.type import *
from parser.expression import *


def generate_c_files(context, output_dir, initial_event, debug=False):

    # Create the renderer
    renderer = Renderer(output_dir)

    renderer.context['debug'] = debug

    # Managing events
    renderer.context['initial_event'] = initial_event
    renderer.context['events'] = dict((event_name, Event(event_name, context, renderer)) for event_name in context.events if event_name != initial_event)
    if initial_event not in renderer.context['events']:
        renderer.context['events']['initial_event'] = Event(initial_event, context, renderer)
    renderer.context['nb_events'] = len(context.events) 

    # Managing tables
    renderer.context['tables'] = dict((table_name, Table(table_name, table, context, renderer)) for table_name, table in context.tables.items())

    # Managing regular expressions
    renderer.context['regexps'] = dict((regexp, Regexp(regexp, context, renderer)) for regexp in context.get_expressions_of_type(MatchExpression))

    # Managing enums
    renderer.context['enums'] = context.enums.values()

    # Managing constants
    renderer.context['constants'] = generate_constants(context)

    renderer.context['include_file'] = 'include_generated.h'


    generate_header_file(context, renderer)
    generate_source_file(context, renderer)


def generate_header_file(context, renderer):
    header_ctx = {}

    # Render the header file
    header_files = [
            ('type.h.tpl', 'type_generated.h'),
            ('event.h.tpl', 'event_generated.h'),
            ('table.h.tpl', 'table_generated.h'),
            ('regexp.h.tpl', 'regexp_generated.h'),
            ('constants.h.tpl', 'constants_generated.h'),
    ]

    renderer.render_template_to_file('include.h.tpl', renderer.context['include_file'], {'headers': [e[1] for e in header_files]})

    for tpl_file, header_name in header_files:
        renderer.render_template_to_file(tpl_file, header_name, header_ctx)


def generate_source_file(context, renderer):
    source_ctx = {}

    # Render the source file
    source_files = [
            ('event.c.tpl', 'event_generated.c'),
            ('table.c.tpl', 'table_generated.c'),
            ('regexp.c.tpl', 'regexp_generated.c'),
            ('constants.c.tpl', 'constants_generated.c'),
    ]

    for tpl_file, source_name in source_files:
        renderer.render_template_to_file(tpl_file, source_name, source_ctx)


def generate_constants(context):
    Constant = namedtuple('Constant', ['name', 'type', 'value'])
    ret = []

    for name, exp in context.var_identifiers.items():
        if exp.__class__.__name__ in ['AnonymEnumExpression']:
            continue

        type_exp = generate_type(exp)
        pre, value, post = generate_c_expression(exp, Rule(None, None, context, None))

        ret.append((name, type_exp, value))

    return ret



def generate_type(type):
    if isinstance(type, Bitstream):
        return 'bitstream_s'
    elif isinstance(type, Integer):
        return 'uint64_t'
    elif isinstance(type, Time):
        return 'long int'
    elif isinstance(type, NetAddr) or isinstance(type, NetAddr4):
        return 'ipv4_addr_s'
    elif isinstance(type, EnumType):
        return type.name + '_e'
    else:
        print "Unknown type for Discus type: %s" % type
        return 'unknown'



class Event:
    def __init__(self, name, context, renderer):
        self.name = name
        self.context = context
        self.renderer = renderer

        # Fetching data from the context
        try:
            self.args = [(arg_name, generate_type(arg_type)) for (arg_name,arg_type) in zip(context.events_args_name[name], context.events_type[name])]
            self.rules = [Rule(rule, self, context, renderer) for rule in context.events[name]]
        except KeyError:
            self.args = []
            self.rules = []

    def get_structure_name(self):
        return '%s_args_s' % self.name.lower()

    def get_structure_full_name(self):
        return 'struct %s' % self.get_structure_name()

    def get_free_function(self):
        return 'free_%s' % self.get_structure_name()


    def get_event_handler(self):
        return 'handle_%s' % self.name.lower()

    def get_alloc_function(self):
        return 'alloc_%s' % self.get_structure_name()

    def get_rules_function(self):
        return ['%s_rule_%d' % (self.name.lower(), idx) for idx in xrange(len(self.rules))]

    def get_rule(self, idx):
        return self.rules[idx]


class Rule:
    def __init__(self, rule, event, context, renderer):
        self.rule = rule
        self.event = event
        self.context = context
        self.renderer = renderer
        self.variables_type = {}
        self.variables_name = defaultdict(int)
        self.variables_value = {}
        self.setup_instructions = []


    def new_variable(self, var_name, var_type, uniq=False):
        if not uniq:
            self.variables_name[var_name] += 1
            var_name = '%s_%d' % (var_name, self.variables_name[var_name])
        self.variables_type[var_name] = var_type
        return var_name


    def get_pointer_args(self):
        return 'args'

    def get_current_args(self):
        return 'current_args'


    def process_abstract_generation(self, gen_func, pre, instr, post):
        # Generate expressions associated to the function
        generated = gen_func()

        # Update expressio
        pre.extend(generated[0])
        instr.extend(generated[1])
        post.extend(generated[2])


    def generate_c_rule(self):
        pre, instr, post = [], [], []

        # 1) Generate let/where expressions
        self.process_abstract_generation(self.generate_let, pre, instr, post)
        self.process_abstract_generation(self.generate_where, pre, instr, post)

        # 2) Generate for/ifnone expression
        # It will manage the inner expression (raises, inserts, updates and so on)
        self.process_abstract_generation(self.generate_for, pre, instr, post)

        # Before returning post, we need to reverse it
        post.reverse()

        return pre + instr + post


    def generate_let(self):
        pre, instr, post = [], [], []

        # Implicit declaration of current args structure
        self.new_variable(self.get_current_args(), '%s *' % self.event.get_structure_full_name(), uniq=True)

        # Declaration of other local variables
        for local_var in self.rule.variables:
            var_name = local_var.name
            var_value = local_var.value
            var_type = var_value.get_type(self.context, self.event)

            # Declare var_name of type var_type
            self.new_variable(var_name, generate_type(var_type), uniq=True)

            # Generating the value
            val_pre, val_instr, val_post = generate_c_expression(var_value, self)
            pre.extend(val_pre)
            post.extend(val_post)

            # Add the instruction to set the value
            pre.append('%s = %s;' % (var_name, val_instr))

        return pre, instr, post



    def generate_where(self):
        pre, instr, post = [], [], []

        if self.rule.where:
            # Generate the condition
            cond_pre, cond_instr, cond_post = generate_c_expression(self.rule.where, self)
            pre.extend(cond_pre)
            post.extend(cond_post)

            # Adding the where expression
            pre.append('if (%s) {' % cond_instr)
            post.append('}')

        return pre, instr, post


    def generate_for(self):
        pre, instr, post = [], [], []

        # First, generate base statements
        stmts_pre, stmts_instr, stmts_post = self.generate_statements()


        if self.rule.for_stmt:
            condition = generate_c_expression(self.rule.for_stmt.exp, self)
            table = self.renderer.context['tables'][self.rule.for_stmt.table_name]

            # Compute ifnone statements
            ifnone_pre, ifnone_instr, ifnone_post = self.generate_ifnone_statements()


            instr.append(self.renderer.render_template('for.tpl', {
                'var_it': self.new_variable('it', 'struct iterator_s'),
                'var_entry': self.new_variable(self.rule.for_stmt.var_name, '%s *' % table.get_structure_full_name(), uniq=True),
                'var_state': self.new_variable('for_state', 'short'),
                'table': table,
                'for_condition' : condition,
                'for_statements' : stmts_pre + stmts_instr + stmts_post,
                'for_first' : self.rule.for_stmt.for_type == 'first',
                'ifnone_statements' : ifnone_pre + ifnone_instr + ifnone_post,
            }))


        else:
            instr.extend(stmts_pre + stmts_instr + stmts_post)

        return pre, instr, post


    def generate_statements(self):
        pre, instr, post = [], [], []
        for stmt in self.rule.all_statements:
            # Generate the statement
            stmt_pre, stmt_instr, stmt_post = generate_c_expression(stmt, self)

            # Updating
            pre.extend(stmt_pre)
            post.extend(stmt_post)
            instr.append(stmt_instr)
        return pre, instr, post

    def generate_ifnone_statements(self):
        pre, instr, post = [], [], []
        for stmt in self.rule.ifnone:
            # Generate the statement
            stmt_pre, stmt_instr, stmt_post = generate_c_expression(stmt, self)

            # Updating
            pre.extend(stmt_pre)
            post.extend(stmt_post)
            instr.append(stmt_instr)
        return pre, instr, post



table_gid = 0

class Table:
    def __init__(self, name, table, context, renderer):
        self.name = name
        self.table = table
        self.context = context
        self.renderer = renderer

        # Set data from the context
        global table_gid
        self.args = [(arg_name, generate_type(arg_type)) for (arg_name,arg_type) in self.table.fields_type.items()]
        self.id = table_gid
        table_gid += 1

    def get_structure_name(self):
        return '%s_table_s' % self.name.lower()

    def get_structure_full_name(self):
        return 'struct %s' % self.get_structure_name()

    def get_structure_size(self):
        return 'sizeof(%s)' % self.get_structure_full_name()


    def get_purge_candidate_fun(self):
        return "%s_entry_purge_candidate" % self.name

	def get_free_entry_fun(self):
		return "%s_free_entry" % self.name

    def get_purge_var_name(self):
        return self.table.purge_stmt[0]

    def get_purge_expression(self):
        return generate_c_expression(self.table.purge_stmt[1], Rule(None, None, self.context, self.renderer))

    def get_free_entry_candidate_fun(self):
        return "%s_free_entry" % self.name

    def get_remove_candidate_fun(self):
        return "%s_entry_remove_candidate" % self.name

    def get_remove_var_name(self):
        return self.table.remove_stmt[0]

    def get_remove_expression(self):
        return generate_c_expression(self.table.remove_stmt[1], Rule(None, None, self.context, self.renderer))


regexp_gid = 0

class Regexp:
    def __init__(self, regexp, context, renderer):
        self.regexp = regexp
        self.context = context
        self.renderer = renderer

        # Unique id for this regexp
        global regexp_gid
        self.id = regexp_gid
        regexp_gid += 1

        self.process_regexp()

    def get_var_name(self):
        return 'regexp_%d' % self.id

    def get_regexp(self):
        return self.needle.__repr__()[1:-1]

    def process_regexp(self):
        self.needle = self.regexp.needle[1:-1]


# ------------------------------------------------------------
# Generation of C expressions
# ------------------------------------------------------------

def convert_expression_name(class_name):
    res = ''
    for c in class_name:
        if c.isupper():
            res += '_'
        res +=  c.lower()
    if res.startswith('_'): res = res[1:]
    return res

def generate_c_expression(expression, rule):
    """
        Abstract function generating c expressions from a discus expression
        It returns a tuple of:
            - pre-expressions: a list of expression to be executed before the expression itself
            - the current expression translated into C
            - post-expressions: a list of expressions to be executed after the expression
    """

    fun = "generate_%s" % convert_expression_name(expression.__class__.__name__)
    if fun in globals():
        return globals()[fun](expression, rule)
    else:
        print '%s might not be implanted yet (line %d)' % (fun, expression.lineno)
        return [], '(not implanted yet)', []


def generate_constant_expression(expression, rule):

    if isinstance(expression.type, Integer):
        return [], str(expression.value), []
    elif isinstance(expression.type, Time) and expression.value == 'now':
        return [], 'current_time(NULL)', []

    print "Constant expression not supported: %s (%s)" % (expression.value, expression.type)
    return [], "(not managed)", []


def generate_arithmetic_operator(expression, rule):
    pre, post = [], []

    # Generating left and right operators
    left_pre, left_instr, left_post = generate_c_expression(expression.op_left, rule)
    right_pre, right_instr, right_post = generate_c_expression(expression.op_right, rule)
    pre.extend(left_pre + right_pre)
    post.extend(left_post + right_post)

    return [], "%s %s %s" % (left_instr, expression.operator, right_instr), []


def generate_identifier_expression(expression, rule):
    if expression.name in [elt[0] for elt in rule.event.args]:
        return [], '%s->%s' % (rule.get_current_args(), expression.name), []

    else:
        return [], expression.name, []


def generate_bitstream_extract_bitstream(expression, rule):
    pre, post = [], []

    # Generate inner instructions
    source_pre, source_instr, source_post = generate_c_expression(expression.name, rule)
    expr_type = expression.get_type(rule.context, rule.rule)
    pre.extend(source_pre)
    post.extend(source_post)

    if isinstance(expr_type, Integer):
        start_pre, start_instr, start_post = generate_c_expression(expression.start, rule)
        end_pre, end_instr, end_post = generate_c_expression(expression.end, rule)
        pre.extend(start_pre + end_pre)
        post.extend(start_post + end_post)

        return [], 'extract_long_unsigned_from_bitstream(&%s, %s, %s)' % (source_instr, start_instr, end_instr), []

    elif isinstance(expr_type, Bitstream):
        # Generate a var name for this extraction
        bitstream_var_name = rule.new_variable('bitstream_extract', 'bitstream_s')
        start_pre, start_instr, start_post = generate_c_expression(expression.start, rule)
        pre.extend(start_pre)
        post.extend(start_post)

        pre.append(rule.renderer.render_template('bitstream_extract.tpl', {
            'var_name' : bitstream_var_name,
            'source': source_instr,
            'start' : start_instr,
        }))

        return pre, bitstream_var_name, post



def generate_in_operator(expression, rule):
    pre, post = [], []

    # There is two case in here
    # 1) address in network
    # 2) value in list of values

    left_type = expression.op_left.get_type(rule.context, rule.rule)
    right_type = expression.op_right.get_type(rule.context, rule.rule)


    if isinstance(left_type, NetAddr) and isinstance(right_type, NetAddr):
        # address in network
        left_pre, left_instr, left_post = generate_c_expression(expression.op_left, rule)
        right_pre, right_instr, right_post = generate_c_expression(expression.op_right, rule)
        pre.extend(left_pre + right_pre)
        post.extend(left_post + right_post)

        # Instruction
        instr = 'ipv4_addr_in_network(&%s, &%s)' % (left_instr, right_instr)
        return pre, instr, post


    elif isinstance(left_type, Integer) and isinstance(right_type, AnyEnumType):
        # value in list of values
        # the list of values is :
        # - a list of values, directly
        # - an identifier of a global variable
        left_pre, left_instr, left_post = generate_c_expression(expression.op_left, rule)
        pre.extend(left_pre)

        if isinstance(expression.op_right, AnonymEnumExpression):
            # Directly a list of values
            anonym_enum = expression.op_right

        elif isinstance(expression.op_right, IdentifierExpression):
            # An identifier to a global variable
            name = expression.op_right.name
            if name not in rule.context.var_identifiers or not isinstance(rule.context.var_identifiers[name], AnonymEnumExpression):
                print '%s is not a valid list of values' % name
                return [], "(not managed)", []
            anonym_enum = rule.context.var_identifiers[name]

        instr = " || ".join(['%s == %d' % (left_instr, val) for val in anonym_enum.values])
        return pre, '(%s)' % instr, post


    else:
        # Unknown operation
        print "in operator not supported: %s in %s" % (left_type, right_type)
        return [], "(not managed)", []




def generate_comparison_operator(expression, rule):
    pre, post = [], []

    # Generating left and right operators
    left_pre, left_instr, left_post = generate_c_expression(expression.op_left, rule)
    right_pre, right_instr, right_post = generate_c_expression(expression.op_right, rule)
    pre.extend(left_pre + right_pre)
    post.extend(left_post + right_post)

    left_type = expression.op_left.get_type(rule.context, rule.rule)
    right_type = expression.op_right.get_type(rule.context, rule.rule)

    if isinstance(left_type, NetAddr) and isinstance(right_type, NetAddr) and expression.operator == '==':
        instr = 'addr_comp(&%s, &%s)' % (left_instr, right_instr)
    else:
        instr = '%s %s %s' % (left_instr, expression.operator, right_instr)


    return pre, instr, post


def generate_logical_operator(expression, rule):
    pre, post = [], []
    ops = {'and': '&&', 'or' : '||'}

    # Generating left and right operators
    left_pre, left_instr, left_post = generate_c_expression(expression.op_left, rule)
    right_pre, right_instr, right_post = generate_c_expression(expression.op_right, rule)
    pre.extend(left_pre + right_pre)
    post.extend(left_post + right_post)

    if isinstance(expression.op_left, ConstantExpression) or isinstance(expression.op_right, ConstantExpression):
        ops = {'and': '&', 'or' : '|'}


    return pre, '(%s) %s (%s)' % (left_instr, ops[expression.operator], right_instr), post


def generate_notexpression(expression, rule):
    return '!(%s)' % generate_c_expression(expression.exp, rule)


def generate_record_access(expression, rule):
    if rule is not None and expression.container in rule.context.enums:
        return [], expression.name, []
    return [], '%s->%s' % (expression.container, expression.name), []

def generate_raise_event(expression, rule):
    pre, post = [], []
    event = rule.renderer.context['events'][expression.name]

    # Computing arguments
    arguments = []
    for arg_name, arg_value in zip(event.args, expression.args):
        arg_pre, arg_instr, arg_post = generate_c_expression(arg_value, rule)
        pre.extend(arg_pre)
        post.extend(arg_post)
        arguments.append((arg_name[0], arg_instr))


    instr = rule.renderer.render_template('raise_event.tpl', {
        'raised_event' : event,
        'var_args' : rule.new_variable('args', '%s *' % event.get_structure_full_name()),
        'arguments': arguments
    })

    return pre, instr, post



def generate_alert_expression(expression, rule):
    instr = 'alert(%s, %s);' % (expression.category, expression.message)
    return [], instr, []


def generate_not_expression(expression, rule):
    instr_pre, instr, instr_post = generate_c_expression(expression.exp, rule)
    return instr_pre, '!( %s )' % instr, instr_post


def generate_table_insert(expression, rule):
    pre, post = [], []
    table = rule.renderer.context['tables'][expression.table]

    # Generating arg values
    assignments = []
    for name, value in expression.assignments:
        val_pre, val, val_post = generate_c_expression(value, rule)
        pre.extend(val_pre)
        post.extend(val_post)
        assignments.append((name.name, val))

    return [], rule.renderer.render_template('insert_table.tpl', {
        'var_insert' : rule.new_variable('table_entry', '%s *' % table.get_structure_full_name()),
        'assignments': assignments,
        'table_id'   : table.id,
        'insert_pre' : pre,
        'insert_post': post,
    }), []



def generate_table_update(expression, rule):
    pre, post = [], []
    # generating left and right operators
    left_pre, left_instr, left_post = generate_c_expression(expression.record, rule)
    right_pre, right_instr, right_post = generate_c_expression(expression.value, rule)
    pre.extend(left_pre + right_pre)
    post.extend(left_post + right_post)

    # Building instruction and returning it
    instr = '%s = %s;\n' % (left_instr, right_instr)
    return pre, instr, post


def generate_match_expression(expression, rule):
    # Retrieve the regexp created for this expression
    regexp = rule.renderer.context['regexps'][expression]
    pre, source, post = generate_c_expression(expression.haystack, rule)

    # We need several variables
    result_var = rule.new_variable('match_res', 'int')


    # We declare new variables for extracted parts
    extracted_vars = []
    for g in expression.groups:
        extracted_vars.append(rule.new_variable(g, 'bitstream_s', True))


    # Setup instructions
    pre.append(rule.renderer.render_template('match.tpl', {
        'result_var' : result_var,
        'haystack': source,
        'regexp_var': regexp.get_var_name(),
        'groups': extracted_vars,
    }))

    return pre, result_var, post


def generate_len(expression, rule):
    pre, bitstream_name, post = generate_c_expression(expression.value, rule)
    return pre, '%s.size' % bitstream_name, post


def generate_to_net4(expression, rule):
    pre, post = [], []
    # generating left and right operators
    addr_pre, addr_instr, addr_post = generate_c_expression(expression.address, rule)
    mask_pre, mask_instr, mask_post = generate_c_expression(expression.mask, rule)

    pre.extend(addr_pre + mask_pre)
    post.extend(addr_post + mask_post)

    # Adding a var
    addr_var = rule.new_variable('addr_ipv4', 'ipv4_addr_s')
    instr = 'init_ipv4_addr(&%s, %s, %s);' % (addr_var, addr_instr, mask_instr)
    pre.append(instr)

    return pre, addr_var, post


def generate_net_addr4(expression, rule):
    value = '{%s, %d}' % ('(%d << 24) + (%d << 16) + (%d << 8) + %d' % tuple(expression.address), expression.mask)
    return [], value, []
