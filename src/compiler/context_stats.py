"""
File: analyzer_deadcode.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: 
"""

import argparse, sys
from parser.context import load_context
from graph.graph import create_graph

def display_enum_stats(context, verbose):
    print 'Enums: %d' % (len(context.enums))

def display_table_stats(context, verbose):
    print 'Tables: %d' % (len(context.tables))

def display_events_stats(context, verbose):
    print 'Events: %d' % (len(context.events))
    nb_rules = sum([len(rules) for rules in context.events.values()])
    print 'Rules: %d' % (nb_rules)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", "-v", action="store_true", help="Display more information about the context")
    parser.add_argument("input", help="Input file ")
    args = parser.parse_args()

    # First, load the context from the input file
    ctx = load_context(open(args.input, 'rb'))

    print 'Statistics of file "%s"\n----' % args.input

    # Compute the statistics and display them
    display_enum_stats(ctx, args.verbose)
    display_table_stats(ctx, args.verbose)
    display_events_stats(ctx, args.verbose)
