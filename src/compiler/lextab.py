# lextab.py. This file automatically created by PLY (version 3.4). Don't edit!
_tabversion   = '3.4'
_lextokens    = {'NET6': 1, 'RAISE': 1, 'OP_LT': 1, 'TIME': 1, 'LEN': 1, 'INTEGER_CST': 1, 'OP_LE': 1, 'MINUS': 1, 'CONCAT': 1, 'ASBIG': 1, 'INSERT': 1, 'OP_NE': 1, 'FLOAT_CST': 1, 'WITH': 1, 'STRING_CST': 1, 'PLUS': 1, 'DOT': 1, 'FORALL': 1, 'INTEGER': 1, 'NET4': 1, 'ASLITTLE': 1, 'TABLE': 1, 'DIVIDE': 1, 'EXISTS': 1, 'INTO': 1, 'ENUM': 1, 'UPDATE': 1, 'TIMES': 1, 'AS': 1, 'IN': 1, 'WHERE': 1, 'ID': 1, 'MATCH': 1, 'OR': 1, 'AND': 1, 'ON': 1, 'OP_EQ': 1, 'FALSE': 1, 'FLOAT': 1, 'OP_GT': 1, 'IPv4_CST': 1, 'ALERT': 1, 'BITSTREAM': 1, 'LET': 1, 'TRUE': 1, 'NOT': 1, 'NOW': 1, 'OP_GE': 1}
_lexreflags   = 0
_lexliterals  = '()[]{}:;,=!<>'
_lexstateinfo = {'INITIAL': 'inclusive'}
_lexstatere   = {'INITIAL': [('(?P<t_IPv4_CST>192\\.168\\.1\\.1)|(?P<t_COMMENT>\\#.*)|(?P<t_INTEGER>int[0-9]+)|(?P<t_FLOAT_CST>-?[0-9]+\\.[0-9]+)|(?P<t_INTEGER_CST>-?[0-9]+)|(?P<t_STRING_CST>"([^\\\\\\n]|(\\\\.))*?")|(?P<t_ID>[a-zA-Z_][a-zA-Z0-9_]*)|(?P<t_newline>\\n+)|(?P<t_OP_EQ>\\=\\=)|(?P<t_CONCAT>\\.\\.)|(?P<t_OP_LE>>\\=)|(?P<t_OP_GE><\\=)|(?P<t_OP_NE>!\\=)|(?P<t_PLUS>\\+)|(?P<t_DOT>\\.)|(?P<t_DIVIDE>\\/)|(?P<t_TIMES>\\*)|(?P<t_MINUS>\\-)|(?P<t_OP_GT><)|(?P<t_OP_LT>>)', [None, ('t_IPv4_CST', 'IPv4_CST'), ('t_COMMENT', 'COMMENT'), ('t_INTEGER', 'INTEGER'), ('t_FLOAT_CST', 'FLOAT_CST'), ('t_INTEGER_CST', 'INTEGER_CST'), ('t_STRING_CST', 'STRING_CST'), None, None, ('t_ID', 'ID'), ('t_newline', 'newline'), (None, 'OP_EQ'), (None, 'CONCAT'), (None, 'OP_LE'), (None, 'OP_GE'), (None, 'OP_NE'), (None, 'PLUS'), (None, 'DOT'), (None, 'DIVIDE'), (None, 'TIMES'), (None, 'MINUS'), (None, 'OP_GT'), (None, 'OP_LT')])]}
_lexstateignore = {'INITIAL': ' \t'}
_lexstateerrorf = {'INITIAL': 't_error'}
