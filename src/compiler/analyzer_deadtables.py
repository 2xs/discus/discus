"""
File: analyzer_deadtables.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Look for useless rules / tables

    A useless table could either be :
        - a table never used
        - a table on which there are only reading
        - a table on which there are only writing
"""

import argparse, sys
import cPickle as pickle
from parser.context import load_context
from graph.graph import create_graph

class TableData:
    def __init__(self, table):
        self.table = table
        self.read = 0
        self.write = 0

    def no_read(self):
        return not self.read

    def add_read(self):
        self.read += 1

    def add_write(self):
        self.write += 1

    def no_write(self):
        return not self.write

    def __repr__(self):
        return '%s [read:%d; write:%d]' % (self.table.name, self.read, self.write)


def generate_table_data(context):
    # Data is stored in a dict
    # That dict contains two lists : read_operations and write_operations
    table_data = {}

    # Initialize the data
    for table_name, table in context.tables.items():
        table_data[table_name] = TableData(table)

    # Fill the data by looking for table operation
    for event_name, rules in context.events.items():
        for rule in rules:
            # Write : insert of update table data
            for insert in rule.inserts:
                table_data[insert.table].add_write()

            for update in rule.updates:
                table_name = rule.set_identifiers[update.table_name]
                table_data[table_name].add_write()

            # Read : Set operation (exists/forall)
            for table_name in rule.set_identifiers.values():
                table_data[table_name].add_read()

    return table_data


def detect_dead_tables(context, data):
    for table_name, table_data in data.items():
        # 1) Look for unused tables
        if table_data.no_read() and table_data.no_write():
            sys.stderr.write('Unused table %s\n' % table_data.table.name)

        # 2) Look for tables with only one operation type (read or write but not both)
        elif table_data.no_read():
            sys.stderr.write('Table %s has only read operations (and no insertion of data)\n' % table_data.table.name)

        elif table_data.no_write():
            sys.stderr.write('Table %s has only write operations (the data is never read)\n' % table_data.table.name)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("--output", "-o", help="output file to store the compiled file (default: stdout)")
    args = parser.parse_args()


    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)

    # Generate table data
    data = generate_table_data(ctx)
    detect_dead_tables(ctx, data)


    #output = sys.stdout if not args.output else open(args.output, 'wb')
    #pickle.dump(ctx, output)
    #output.close()



