"""
File: analyzer_loopdetector.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Event loop detector
"""

import argparse, sys
from parser.context import load_context
from graph.graph import create_graph


def detect_loops(context):
    # First, generate the graph from the context
    graph = create_graph(context)

    # Launch the detection with initial entry points
    for entry in graph.get_roots():
        process_node(context, graph, entry, [])

def process_node(context, graph, node, visited_nodes):
    if node in graph.nodes:
        visited_nodes = visited_nodes[:]
        visited_nodes.append(node)

        for successor in graph.get_successors(node):
            # We check whether or not we already know or not the successor
            # If we already know it, it means we have detected a loop
            if successor in visited_nodes:
                # Loop detected
                visited_nodes.append(successor)
                context.add_error('Event loop detected : %s' % (' -> '.join(visited_nodes[visited_nodes.index(successor):])))

            else:
                # We continue the inspection
                process_node(context, graph, successor, visited_nodes)


    else:
        print 'unknown node'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    args = parser.parse_args()


    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)

    detect_loops(ctx)

    if len(ctx.errors):
        ctx.print_errors()
        sys.exit(0)


