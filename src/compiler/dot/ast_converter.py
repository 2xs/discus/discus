'''
File: dot.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Dot converter for discus
    Generate the input language structure as a serie of graphes
'''

import os
import pydot

# Names used to identify one dot node
names = {}

class Converter:

    def __init__(self, context, output):
        self.ctx = context
        self.output = output


    def convert(self, png, dot):
        graph = pydot.Dot(graph_type='digraph')

        # We need to generate all the elements of the input file (enums, tables, events and so on)
        # 1) Generate enums
        for enum in self.ctx.enums.values():
            graph.add_node(build_node(graph, enum))

        # 2) Generate tables
        for table in self.ctx.tables.values():
            graph.add_node(build_node(graph, table))

        # 3) Generate events
        for event_name, event_rules in self.ctx.events.items():
            # For each event, generate a top event that only contains the name of the event
            top_event = gen_record_node(gen_node_id('event'), event_name)
            graph.add_node(top_event)

            for rule in event_rules:
                rule = build_node(graph, rule)
                graph.add_node(rule)
                add_edge(graph, top_event, rule)

        if png: graph.write_png(self.output + '.png')
        if dot: graph.write_dot(self.output + '.dot')



##### ENUM/TABLE GENERATION

def build_table(graph, node):
    fields_name = ' | '.join(node.fields_type)
    fields_type = ' | '.join([get_type_repr(f.type) for f in node.fields])

    node_content = " { table %s | { {%s} | {%s} } } " % (node.name, fields_name, fields_type)
    return gen_record_node(gen_node_id(get_node_type(node)), node_content)



def build_enum(graph, node):
    fields_name = ' | '.join([elt[0] for elt in node.keys()])
    fields_value = ' | '.join([str(elt) for elt in node.fields_dict.values()])
    node_content = " { enum %s | { {%s} | {%s} } } " % (node.name, fields_name, fields_value)
    return gen_record_node(gen_node_id(get_node_type(node)), node_content)



##### EVENT RELATED FUNCTIONS

def build_event(graph, node):
    # Event node
    event_name = gen_node_id(get_node_type(node))
    event = gen_record_node(event_name, " { { <args> args | <where>where | <insert>insert | <update>update| <raised>raised events } }")

    # Arguments node
    args_name = ' | '.join([arg.name for arg in node.args])
    args_type = ' | '.join([get_type_repr(arg.type) for arg in node.args])
    args_content = " { { {%s} | {%s} } } " % (args_name, args_type)
    args_node = gen_record_node(gen_node_id('%s_args' % event_name), args_content)
    graph.add_node(args_node)
    add_edge(graph, '%s:args' % event_name, args_node)

    # Where node
    if node.where is not None:
        where_node = build_node(graph, node.where)
        graph.add_node(where_node)
        add_edge(graph, '%s:where' % event_name, where_node)

    # Insert nodes
    for insert in node.inserts:
        insert_node = build_node(graph, insert)
        graph.add_node(insert_node)
        add_edge(graph, '%s:insert' % event_name, insert_node)

    # Update node
    if len(node.updates):
        updates_name = ' | '.join(['{ %s.%s | <up%d>= }' % (update.table_name, update.field_name, idx) for idx, update in enumerate(node.updates)])
        update_name = gen_node_id('update')
        update_content = " { update | { %s } } " % updates_name
        update_node = gen_record_node(update_name, update_content)
        graph.add_node(update_node)
        add_edge(graph, '%s:update' % event_name, update_node)

        for idx, update in enumerate(node.updates):
            value_node = build_node(graph, update.value)
            graph.add_node(value_node)
            add_edge(graph, '%s:up%d' % (update_name, idx), value_node)

    # Event to be raised
    if len(node.raised_events):
        for raised_event in node.raised_events:
            raised_name = raised_event.name
            raised_args = raised_event.args
            raised_delay = raised_event.delay

            content = [raised_name]

            # Managing args
            if len(raised_args):
                args_str = ' | '.join(['<arg%d>' % idx for idx, arg in enumerate(raised_args)])
                args_str = " { args | { %s } } " % args_str
                content.append(args_str)

            content.append('in')
            content.append('%d' % raised_delay)

            raised_content = " { { %s } } " % (' | '.join(content))
            raised_name = gen_node_id(raised_name)
            raised_node = gen_record_node(raised_name, raised_content)
            graph.add_node(raised_node)
            add_edge(graph, '%s:<raised>' % event_name, raised_node)

            # Creating args
            for idx, arg in enumerate(raised_args):
                arg_node = build_node(graph, arg)
                graph.add_node(arg_node)
                add_edge(graph, '%s:<arg%d>' % (raised_name, idx), arg_node)

    return event


def build_tableinsert(graph, node):
    fields_name = ' | '.join(['{ %s | <field%d>= }' % (assign[0].name, idx) for idx, assign in enumerate(node.assignments)])
    insert_name = gen_node_id('tableinsert')
    insert_content = ' { insert into %s | { %s } } ' % (node.table, fields_name)
    insert_node = gen_record_node(insert_name, insert_content)

    for idx, assign in enumerate(node.assignments):
        value_node = build_node(graph, assign[1])
        graph.add_node(value_node)
        add_edge(graph, '%s:field%d' % (insert_name, idx), value_node)

    return insert_node


##### BINARY EXPRESSION

def binary_operator(graph, left, op, right):
    # Create a node for the operator
    op_content = " { { %s } | { <left> | <right> } } " % op
    op_name = gen_node_id('binary_op')
    op_node = gen_record_node(op_name, op_content)
    graph.add_node(op_node)

    # Left and right operand
    left_node = build_node(graph, left)
    right_node = build_node(graph, right)
    graph.add_node(left_node)
    graph.add_node(right_node)

    # Link nodes
    add_edge(graph, '%s:<left>' % op_name, left_node)
    add_edge(graph, '%s:<right>' % op_name, right_node)
    return op_node

def build_comparisonoperator(graph, node):
    return binary_operator(graph, node.op_left, node.operator, node.op_right)

def build_arithmeticoperator(graph, node):
    return binary_operator(graph, node.op_left, node.operator, node.op_right)

def build_logicaloperator(graph, node):
    return binary_operator(graph, node.op_left, node.operator, node.op_right)

def build_networkaddressbelongto(graph, node):
    return binary_operator(graph, node.op_left, 'in', node.op_right)


##### SET OPERATION
def build_set_operation(graph, node, operation):
    set_content = " { { %s } | { %s | in | %s | <with> with } } " % (operation, node.var_name, node.table_name)
    set_name = gen_node_id(get_node_type(node))
    set_node = gen_record_node(set_name, set_content)

    # Condition node
    cond_node = build_node(graph, node.condition)
    graph.add_node(cond_node)
    add_edge(graph, '%s:<with>' % (set_name), cond_node)

    return set_node

def build_existsoperation(graph, node):
    return build_set_operation(graph, node, 'exists')

def build_foralloperation(graph, node):
    return build_set_operation(graph, node, 'forall')


##### MATCH OPERATION
def build_matchexpression(graph, node):
    content = ['<haystack>', 'match', node.needle]

    # Managing groups
    if len(node.groups):
        content.append('as')
        content.append(' { %s }' % ' | '.join(node.groups))

    match_name = gen_node_id(get_node_type(node))
    match_node = gen_record_node(match_name, " { { %s } } " % ' | '.join(content))

    haystack_node = build_node(graph, node.haystack)
    graph.add_node(haystack_node)
    add_edge(graph, '%s:<haystack>' % match_name, haystack_node)
    return match_node


##### BITSTREAM OPERATION

def build_bitstreamextractbitstream(graph, node):

    container = build_node(graph, node.name)

    node_content = " { extract range | { <start> | <end> } } "
    extract_name = gen_node_id(get_node_type(node))
    extract_node = gen_record_node(extract_name, node_content)
    graph.add_node(extract_node)

    add_edge(graph, container, extract_node)

    # Generate start and end nodes
    if node.start is not None:
        start_node = build_node(graph, node.start)
        graph.add_node(start_node)
        add_edge(graph, '%s:<start>' % extract_name, start_node)

    if node.end is not None:
        end_node = build_node(graph, node.end)
        graph.add_node(end_node)
        add_edge(graph, '%s:<end>' % extract_name, end_node)

    return container


def build_bitstreamextractbit(graph, node):

    container = build_node(graph, node.name)

    node_content = " { extract bit | { <index> } } "
    extract_name = gen_node_id(get_node_type(node))
    extract_node = gen_record_node(extract_name, node_content)
    graph.add_node(extract_node)

    add_edge(graph, container, extract_node)

    end_node = build_node(graph, node.index)
    graph.add_node(end_node)
    add_edge(graph, '%s:<index>' % extract_name, end_node)

    return container




##### BASIC EXPRESSION RELATED FUNCTIONS

def build_constantexpression(graph, node):
    return gen_record_node(gen_node_id('constant'), str(node.value))

def build_identifierexpression(graph, node):
    return gen_record_node(gen_node_id('identifier'), node.name)

def build_recordaccess(graph, node):
    node_content = " { { %s | . | %s } }" % (node.container, node.name)
    return gen_record_node(gen_node_id('recordaccess'), node_content)


##### NETWORK ADDRESS RELATED NODES

def build_net4(graph, node):
    net4_content = " { Net4 | { <addr>address | <mask>mask } } "
    net4_name = gen_node_id(get_node_type(node))

    # addr node
    addr_node = build_node(graph, node.address)
    graph.add_node(addr_node)
    add_edge(graph, '%s:<addr>' % net4_name, addr_node)

    # mask node
    mask_node = build_node(graph, node.mask)
    graph.add_node(mask_node)
    add_edge(graph, '%s:<mask>' % net4_name, mask_node)

    return gen_record_node(net4_name, net4_content)



##### GRAPH RELATED FUNCTIONS

def add_edge(graph, source, dest):
    graph.add_edge(pydot.Edge(source,dest))

def gen_node_id(name):
    global names
    if name not in names:
        names[name] = 1
    gen_id = "%s%d" % (name, names[name])
    names[name] += 1
    return gen_id

def gen_record_node(content_id, content):
    return pydot.Node(content_id, label=content, shape='Mrecord')

def build_node(graph, node):
    """
        Build a node
        According to the name of the node, either it calls a corresponding function or
        it calls a default function
    """
    fun = 'build_%s' % get_node_type(node)
    if fun in globals():
        return globals()[fun](graph, node)
    else:
        return build_node_default(graph, node)

def build_node_default(graph, node):
    print 'build_%s might not be implanted yet' % get_node_type(node)
    return pydot.Node(gen_node_id(get_node_type(node)))


##### NODE RELATED FUNCTIONS

def get_type_repr(node_type):
    return node_type.__class__.__name__.lower()

def get_node_type(node):
    return node.__class__.__name__.lower()

