'''
File: graph_converter.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Dot converter for discus
    Generate the graph of events
'''

import os
import pydot

names = {}

class Converter:

    def __init__(self, graph, output):
        self.graph = graph
        self.output = output


    def convert(self, png, dot):
        dot_graph = pydot.Dot(graph_type='digraph')

        # We need to generate the node for every event
        for event in self.graph.nodes:
            self.build_event(dot_graph, event)

        if png: dot_graph.write_png(self.output + '.png')
        if dot: dot_graph.write_dot(self.output + '.dot')


    def build_event(self, dot_graph, event):
        # Building the node
        node = pydot.Node(event, label=event, shape='Mrecord')
        dot_graph.add_node(node)

        # Creating edges
        for successor in self.graph.get_successors(event):
            dot_graph.add_edge(pydot.Edge(node,successor))
