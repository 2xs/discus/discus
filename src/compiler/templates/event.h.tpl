{% extends "base.tpl" %}

{% macro generate_event_struct(event) -%}
/* Event {{ event.name }} : arguments' structure */
{{ event.get_structure_full_name() }} {
    {% for field_name, field_type in event.args %}
    {{ field_type }} {{ field_name }};
    {% endfor %}
};
{%- endmacro %}


{% macro generate_event_memory_functions(event) -%}
/* Event {{ event.name }} : free/alloc functions */
void * {{ event.get_alloc_function() }}();
void {{ event.get_free_function() }}(void * args);
{%- endmacro %}


{% block content %}
#ifndef GENERATED_EVENT_H
#define GENERATED_EVENT_H

#include "core.h"
#include "{{ include_file }}"


{% for event in events.values() %}
/* -------- Event {{ event.name }} -------- */
{% if event.name != initial_event %}
    {{ generate_event_struct(event) }}
    {{ generate_event_memory_functions(event) }}
{% endif %}

/* Event {{ event.name }} : process handler and rules' function*/
void {{ event.get_event_handler() }}(void * args);
{% for rule_function in event.get_rules_function() %}
void {{ rule_function  }}(void * args);
{% endfor %}
{% endfor %}


#endif
{% endblock %}
