#ifndef GENERATED_CONTANTS_H
#define GENERATED_CONTANTS_H

#include "core.h"

/* Declaration of constants */
{% for name, type, value in constants %}
    extern {{ type }} {{ name }};
{% endfor %}

#endif

