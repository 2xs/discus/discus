{% extends "base.tpl" %}

{% macro generate_table(table) -%}
    {% set purge_pre, purge_exp, purge_post = table.get_purge_expression() %}
    {% set remove_pre, remove_exp, remove_post = table.get_remove_expression() %}

    /* Table {{ table.name }} : fields' structure */
    {{ table.get_structure_full_name() }} {
        {% for field_name, field_type in table.args %}
            {{ field_type }} {{ field_name }};
        {% endfor %}
    };

    /* Table {{ table.name }} : purge candidate inline function */
    inline static int {{ table.get_purge_candidate_fun() }}(void * entry_ptr)
    {
        {{ table.get_structure_full_name() }} * {{ table.get_purge_var_name() }} = ({{ table.get_structure_full_name() }} *) entry_ptr;
        return ({{ purge_exp }});
    }

    /* Table {{ table.name }} : delete candidate inline function */
    inline static int {{ table.get_remove_candidate_fun() }}(void * entry_ptr)
    {
        {{ table.get_structure_full_name() }} * {{ table.get_purge_var_name() }} = ({{ table.get_structure_full_name() }} *) entry_ptr;
        {{ remove_pre|join('\n') }}
        return ({{ remove_exp }});
    }

    /* Table {{ table.name }} : free entry candidate inline function */
    inline static short {{ table.get_free_entry_candidate_fun() }}(void * entry_ptr)
    {
	{{ table.get_structure_full_name() }} * entry = ({{ table.get_structure_full_name() }} *) entry_ptr;
	mem_free(entry);

	return 0;
	}



{%- endmacro %}

{% block content %}
#ifndef GENERATED_TABLE_H
#define GENERATED_TABLE_H

#include "platform.h"
#include "core.h"
#include "{{ include_file }}"


void table_init();

{% if tables | count > 0 %}
struct table_s tables_info[{{ tables|count }}];
{% endif %}

{% for table in tables.values() %}
    {{ generate_table(table) }}
{% endfor %}


#endif
{% endblock %}
