{% block content %}
#ifndef GENERATED_TYPE_H
#define GENERATED_TYPE_H

{% for enum in enums %}
typedef enum {{ enum.name }} {
    {% for key, val in enum.fields_list %}
        {{ key }} = {{ val }},
    {% endfor %}
} {{enum.name}}_e ;

{% endfor %}

#endif
{% endblock %}
