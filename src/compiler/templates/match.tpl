/* Match operation (FIXME: support only byte aligned data for now) */
	const char *ptr = (const char*){{ haystack }}.smart_buffer->buffer + ({{haystack}}.offset/8);
	unsigned int match_size = {{ haystack }}.size/8;
{{ result_var }} = pcre_exec(
        {{ regexp_var }},                                   /* Compiled expression */
        NULL,                                               /* No extra */
        ptr, /* Haystack (where to look for the needle) */
		match_size,                                /* Size of haystack */
        0,                                                  /* Start looking at ... */
        0,                                                  /* Options */
        NULL,                                               /* No extraction */
        0                                                   /* Length of the extraction tab */
);

/* Extract data from the result */
{% if groups %}
    if ({{ result_var }} > 0)
    {
        {% for group in groups %}
            /* Extracting {{ group }} */
            if (extract_bitstream_from_bitstream(&{{ haystack }}, &{{ group }}, 0, 0) == NULL)
            {
                /* The bitstream cannot be extract */
                log_char(2, "Aborded rule: extract bitstream ");
                log_char(2, __FILE__);
                log_int(2, __LINE__);
                log_char(2, "\n");

                return;
            }
        {% endfor %}
    }
{% endif %}


/* We want {{ result_var }} to contain whether or not the string was found */
{{ result_var }} = {{ result_var }} >= 0;
if (!{{ result_var }})
    return;

