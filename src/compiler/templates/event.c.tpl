{% extends "base.tpl" %}


{% macro generate_free_function(event) -%}
void {{ event.get_free_function() }}(void * args)
{
    {% set args_struct_declared = False %}
    {% for field_name, field_type in event.args %}
        {% if field_type == 'bitstream_s' %}
            {% if not args_struct_declared %}
    {{ event.get_structure_full_name() }}* data = ({{ event.get_structure_full_name() }}*) args;
            {% endif %}
    free_bitstream(&data->{{field_name }});
        {% set args_struct_declared = True %}
        {% endif %}
    {% endfor %}
    free(args);
}
{%- endmacro %}

{% macro generate_alloc_function(event) -%}
void * {{ event.get_alloc_function() }}()
{
    return mem_malloc(sizeof({{ event.get_structure_full_name() }}));
}
{%- endmacro %}


{% macro generate_rule_function(rule_name, rule) -%}
void {{ rule_name }}(void * {{ rule.get_pointer_args() }})
{

    {% set instr = rule.generate_c_rule() %}

    /* Variables */
    {% for var_name, var_type in rule.variables_type.iteritems() %}
        {{ var_type }} {{ var_name }};
    {% endfor %}

    /* Initialize arguments' structure */
    {{ rule.get_current_args() }} = ({{ rule.event.get_structure_full_name() }} *) {{ rule.get_pointer_args() }};

    {% for instr in instr %}
        {{ instr }}
    {% endfor %}

}
{%- endmacro %}


{% block content %}
#include <pcre.h>

#include "platform.h"
#include "core.h"
#include "{{ include_file }}"

{% for event in events.values() %}
    /* -------- Event {{ event.name }} -------- */
    {% if event.name != initial_event %}
        /* Alloc/free functions */
        {{ generate_free_function(event) }}
        {{ generate_alloc_function(event) }}
    {% endif %}


    /* Handle function */
    void {{ event.get_event_handler() }}(void * args)
    {

        {% if event.name == initial_event %}
        {% endif %}

        {% if debug %}
            log_char(2, ">> Event {{ event.name }}\n");
        {% endif %}

        /* Sequential process of {{ event.name }}'s rules */
        {% for rule_name in event.get_rules_function() %}
            {{ rule_name }}(args);
        {% endfor %}

        /* Free the arguments' structure */
        {{ event.get_free_function() }}(args);

        {% if debug %}
            log_char(stderr, "<< Event {{ event.name }}\n");
        {% endif %}
    }


    /* Rules' function */
	{{ for_cond }}	

    {% for rule_name in event.get_rules_function() %}
        {{ generate_rule_function(rule_name, event.get_rule(loop.index0)) }}
    {% endfor %}
{% endfor %}

{% endblock %}
