#ifndef GENERATED_INCLUDE_H
#define GENERATED_INCLUDE_H

{% for header_name in headers %}
    #include "{{ header_name }}"
{% endfor %}

#endif

