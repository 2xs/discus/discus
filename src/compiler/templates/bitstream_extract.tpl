/* Creating bitstream {{ var_name }} from {{ source }} */
if (extract_bitstream_from_bitstream(&{{ source }}, &{{ var_name }}, {{ start }}, {{ source }}.size) == NULL)
{
    /* The bitstream cannot be extract */
    log_char(2, "Aborded rule: extract bitstream ");
    log_char(2, __FILE__);
    log_int(2, __LINE__);
    log_char(2, "\n");

    return;
}
