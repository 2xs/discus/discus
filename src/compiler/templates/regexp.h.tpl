{% extends "base.tpl" %}

{% block content %}
#ifndef GENERATED_REGEXP_H
#define GENERATED_REGEXP_H

#include <pcre.h>

void regexp_init();
void regexp_free();

{% for regexp in regexps.values() %}
    pcre * {{ regexp.get_var_name() }}; /* Expression regular (line {{ regexp.regexp.lineno }}) */
{% endfor %}

#endif
{% endblock %}
