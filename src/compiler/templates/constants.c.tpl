#ifndef GENERATED_CONTANTS_H
#define GENERATED_CONTANTS_H

#include "constants_generated.h"
#include "platform.h"
#include "core.h"

/* Declaration of constants */
{% for name, type, value in constants %}
    {{ type }} {{ name }} = {{ value }};
{% endfor %}

#endif

