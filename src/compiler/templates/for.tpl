{% set for_pre, for_cond, for_post = for_condition -%}


/* Initializing for statement */
init_iterator({{ table.id}}, &{{ var_it }});
{{ var_entry }} = ({{ table.get_structure_full_name() }} *) {{ var_it }}.current_entry;
{{ var_state }} = 0;

/* Looping over entries from the table */
while ({{ var_entry }} != NULL)
{
    {% if for_pre %}
        {{ for_pre|join('\n') }}
    {% endif %}

    if ({{ for_cond }})
    {
        {{ for_statements|join('\n') }}

        /* Update the loop state */
        {{ var_state }} = 1;
        {% if for_first %}
            /* We only want to process the first value, exit the loop */
            break;
        {% endif %}
    }

    {% if for_post %}
        {{ for_post|join('\n') }}
    {% endif %}

    {{ var_entry }} = ({{ table.get_structure_full_name() }} *) iterator_next(&{{ var_it }});
}

{% if ifnone_statements %}
if (!{{ var_state}})
{
        {{ ifnone_statements|join('\n') }}
}
{% endif %}
