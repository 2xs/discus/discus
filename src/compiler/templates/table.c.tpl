{% extends "base.tpl" %}


{% block content %}

#include "platform.h"
#include "core.h"
#include "{{ include_file }}"

void table_init()
{
    {% for table in tables.values() %}
        create_table({{ table.id }}, {{ table.get_structure_size() }}, {{ table.get_purge_candidate_fun() }}, {{ table.get_remove_candidate_fun() }}, {{ table.get_free_entry_candidate_fun() }});
    {% endfor %}
}


{% endblock %}
