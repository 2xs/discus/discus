/* Raise event {{ raised_event.name }} */
    /* Initialize event structure */

    /* Allocating the argument structure */
    if (({{ var_args }} = {{ raised_event.get_alloc_function() }}()) == NULL)
    {
        log_char(2, "Couldn't allocate memory space\n");
    } else {
        /* Initializing arguments */
        {% for arg_name, arg_value in arguments %}
        {{ var_args }}->{{ arg_name }} = {{ arg_value }};
        {% endfor %}


        /* Trying to push the event into the main queue */
        if (!queue_push(&queue, {{ raised_event.get_event_handler() }}, {{ raised_event.get_free_function() }}, {{ var_args }}))
        {
            /* The event was not pushed and need to be freed */
            {{raised_event.get_free_function() }}({{ var_args }});

        }
    }
