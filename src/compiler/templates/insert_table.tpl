/* Insert statement */
{{ insert_pre|join('\n') }}


/* Inserting the data into the table */
if (({{ var_insert }} = insert_entry({{ table_id }})) == NULL)
{
    log_char(2, "Entry insertion into table {{ table_id }} failed ...\n");
} else {
/* Initializing {{ var_insert }} variable */
{% for field_name, field_value in assignments %}
    {{ var_insert }}->{{ field_name }} = {{ field_value }};
{% endfor %}
}


{% if debug %}
    log_char(2, "Successful insertion into table {{ table_id }}\n");
{% endif %}

{{ insert_post|join('\n') }}
