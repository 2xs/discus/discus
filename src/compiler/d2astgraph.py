"""
File: ast.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Generate the AST of the Discus input file
"""

import argparse, sys
from parser import context
from dot import ast_converter

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("output",  help="Output file (without extension)")
    parser.add_argument("--png", action="store_true", help="Produces a png file")
    parser.add_argument("--dot", action="store_true", help="Produces a dot file")
    args = parser.parse_args()

    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = context.load_context(input)

    # Load the context into the dot converter
    dot = ast_converter.Converter(ctx, args.output)
    dot.convert(args.png, args.dot)
