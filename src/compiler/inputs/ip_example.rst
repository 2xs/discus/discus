# ------------ Ethernet packet ---------------
enum Ethertype {
    IPv4 = 0x0800,
    ARP = 0x0806
};

on Packet(bitstream p)
    raise EthernetParser(p);

on EthernetParser(bitstream p)
    let mac_dst = p[64:111]
    let mac_src = p[112:159]
    let ethertype = enum(Ethertype, p[160:175])
    let payload = p[176:-33]
    raise EthernetPacket(mac_src, mac_dst, ethertype, payload);

    on EthernetPacket(int48 mac_src, int48 mac_dst, enum Ethertype ethertype, bitstream payload)
    where ethertype == Ethertype.IPv4
    raise IPv4Parser(payload);



# ------------ IPv4 ---------------
# IPv4 Parser
# Dissassemble a packet and raise an IPv4packet event
on IPv4Parser(bitstream p)
    let ihl = p[4:7]
    where p[0:3] == 4
    raise IPv4Packet(
        p[16:31],                   # Length of the packet
        p[32:47],                   # Packet ID
        p[49],                      # Don't Fragment
        p[50],                      # More Fragment
        p[51:64],                   # Fragment offset
        p[64:71],                   # TTL
        p[72:79],                   # Protocol
        p[80:95],                   # Checksum
        p[96:127],                  # Source IP
        p[128:159],                 # Destination IP
        p[160:32 * ihl - 1],        # Options
        p[32 * ihl:]                # Payload
    );


on IPv4Packet(int16 length, int16 ipid, int1 df,
              int1 mf, int13 fragment_offset, int8 ttl,
              int8 proto, int16 checksum, int32 ip_src,
              int32 ip_dst, bitstream options, bitstream payload)
    where payload[0:3] in Ethertype
    alert("tcp", "message", start_time=now)
    ;


on IPv4Packet(int16 length, int16 ipid, int1 df,
              int1 mf, int13 fragment_offset, int8 ttl,
              int8 proto, int16 checksum, int32 ip_src,
              int32 ip_dst, bitstream options, bitstream payload)
    raise TestEvent();

on TestEvent()
    raise TestLeave();

on TestLeave();

