"""
File: analyzer_deadcode.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Detect dead code (useless events or rules)
"""

import argparse, sys
import cPickle as pickle
from parser.context import load_context
from graph.graph import create_graph


def detect_unreachable_events(context, entry_points):
    """
        Look for unreachable events and labels them as useless events
    """
    # First, generate the graph from the context
    graph = create_graph(context)

    # Doing a breadth first search to look for all reachable events
    reachable_events = entry_points
    current_events = entry_points
    loop_index = 1

    while len(current_events):
        new_events = []

        for event in current_events:
            for successor in graph.get_successors(event):
                if successor not in reachable_events:
                    new_events.append(successor)
                    reachable_events.append(successor)

        current_events = new_events
        loop_index += 1

    # Looking for unreachable events and returns them
    return [(event_name, 'remove', 'Unreachable event') for event_name in graph.nodes if event_name not in reachable_events]

def detect_dead_branches(context):
    """
        Look for dead branches
        A dead branch is a sequence of events that leads to a leave that does nothing
    """
    # First, generate the graph from the context
    graph = create_graph(context)

    current_events = graph.get_leaves()
    action_events = set()
    loop_index = 1

    while len(current_events):
        new_events = []

        for event in current_events:
            if context.trigger_actions(event):
                # We can colorize the whole branch from here
                action_events.add(event)
                action_events.update(graph.get_ancestors(event))

            else:
                # We need to inspect its predecessors
                new_events.extend([predecessor for predecessor in graph.get_predecessors(event) \
                        if predecessor not in action_events and predecessor not in new_events])

        current_events = new_events
        loop_index += 1

    # Looking for events not leading to an action
    return [(event_name, 'remove', 'Not leading to an action') for event_name in graph.nodes if event_name not in action_events]


def detect_dead_rules(context):
    """
        Look for dead rules
        A dead rule is a rule doing nothing :
        it does not raise another event, insert or update data or alerts
    """
    useless_rules = []

    for event, rules in context.events.items():
        for rule in rules:
            if not rule.trigger_actions() and not len(rule.raised_events):
                useless_rules.append((rule, 'remove', 'Not doing anything'))

    return useless_rules


def label_dead_events(context, dead_events):
    sys.stderr.write('Dead code analyzer : %d events reported\n' % (len(dead_events)))
    for event_name, category, message in dead_events:
        sys.stderr.write('  - %s : %s\n' % (event_name, message))
        context.add_event_label(event_name, category, message)

def label_dead_rules(dead_rules):
    sys.stderr.write('Dead code analyzer : %d rules reported\n' % (len(dead_rules)))
    for rule, category, message in dead_rules:
        sys.stderr.write('  - %s (line %d) : %s\n' % (rule.name, rule.lineno, message))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("--output", "-o", help="output file to store the compiled file (default: stdout)")
    parser.add_argument("entry", nargs='+', help="Entry point(s) of the graph")
    args = parser.parse_args()


    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)

    # Detect useless events
    dead_events = detect_unreachable_events(ctx, args.entry)
    dead_events += detect_dead_branches(ctx)
    label_dead_events(ctx, dead_events)

    # Detect useless rules
    dead_rules = detect_dead_rules(ctx)
    label_dead_rules(dead_rules)


    output = sys.stdout if not args.output else open(args.output, 'wb')
    pickle.dump(ctx, output)
    output.close()
