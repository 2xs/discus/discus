"""
File: renderer.py
Author: Damien Riquet
Email: d.riquet@gmail.com
Description: Render C files using Jinja
"""

import os
import re
from jinja2 import Environment, FileSystemLoader

class Renderer:
    def __init__(self, output_dir, template_dir="templates/"):
        self.template_dir = template_dir
        self.output_dir = output_dir
        self.env = Environment(loader=FileSystemLoader(template_dir), lstrip_blocks=True, trim_blocks=True)
        self.context = {}


    def render_template_to_file(self, template_name, output_filename, data):
        rendered = self.render_template(template_name, data)

        with open(os.path.join(self.output_dir, output_filename), 'w') as f:
            f.write(rendered)


    def render_template(self, template_name, data):
        template = self.env.get_template(template_name)
        data.update(self.context)
        rendered = template.render(data)

        return rendered

