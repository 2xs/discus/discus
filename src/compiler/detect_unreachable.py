import sys
import argparse
from graph.graph import create_graph
from parser.yacc import parse_file

def detect_unreachable_events(context, entry_points):
    """
        Look for unreachable events
    """
    # First, generate the graph from the context
    graph = create_graph(context)

    # Doing a breadth first search to look for all reachable events
    reachable_events = entry_points
    current_events = entry_points
    loop_index = 1

    while len(current_events):
        new_events = []

        for event in current_events:
            for successor in graph.get_successors(event):
                if successor not in reachable_events:
                    new_events.append(successor)
                    reachable_events.append(successor)

        current_events = new_events
        loop_index += 1

    # Looking for unreachable events and returns them
    return [event_name for event_name in graph.nodes if event_name not in reachable_events]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input file")
    parser.add_argument("entry", nargs='+', help="Entry point(s) of the graph")
    args = parser.parse_args()


    # Open the file
    ctx = parse_file(args.input)

    unreachable_events = detect_unreachable_events(ctx, args.entry)

    for event in unreachable_events:
        print "Unreachable event: %s" % event

    sys.exit(len(unreachable_events))

