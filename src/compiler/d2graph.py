"""
File: ast.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Generate the AST of the Discus input file
"""

import argparse, sys
from parser.context import load_context
from dot.graph_converter import Converter
from graph.graph import create_graph

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("output",  help="Output file (without extension)")
    parser.add_argument("--png", action="store_true", help="Produces a png file")
    parser.add_argument("--dot", action="store_true", help="Produces a dot file")
    args = parser.parse_args()

    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)
    graph = create_graph(ctx)

    # Load the graph into the dot converter
    converter = Converter(graph, args.output)
    converter.convert(args.png, args.dot)


