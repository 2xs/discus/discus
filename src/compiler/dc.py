'''
File: dc.py
Author: Damien Riquet <d.riquet@gmail.com>
Description: Discus Compiler
    Parse a Discus file and interpret it
    then generate a serialized context from it
'''

import argparse, sys
import cPickle as pickle
from parser.yacc import parse_file

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("source_file", help="Discus source file to be compiled")
    parser.add_argument("--output", "-o", help="output file to store the compiled file (default: stdout)")
    args = parser.parse_args()

    # Open the file
    ctx = parse_file(args.source_file)

    # Generate dot output
    if ctx is not None:

        ctx.verify_context()

        if len(ctx.errors):
            ctx.print_errors()
            sys.exit(1)

        output = sys.stdout if not args.output else open(args.output, 'wb')
        pickle.dump(ctx, output)
        output.close()


