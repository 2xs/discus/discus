"""
File: main.py
Author: Damien Riquet
Email: d.riquet@gmail.com
Description: Generate C files from a Discus context
"""

import argparse, sys
from subprocess import call

from parser.context import load_context
from generator import generate_c_files



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input file (default stdin)")
    parser.add_argument("--initial", default="ethernet_packet_raw", help="Initial event (default: ethernet_packet_raw)")
    parser.add_argument("--debug", "-d", action="store_const", const=True, default=False, help="Debug mode")
    parser.add_argument("output",  help="Output directory")
    args = parser.parse_args()

    # Managing input/output
    input = sys.stdin if not args.input else open(args.input, 'rb')

    # First, load the context from the input file
    ctx = load_context(input)

    # Generate and render c files
    generate_c_files(ctx, args.output, args.initial, args.debug)

    # [Optional] Pretty print the files
    # Check if astyle is installed
    ret = call ("which -s astyle", shell=True)
    if ret == 0:
        # If so, run it
        ret = call("astyle -n %s/*.[ch]" % args.output, shell=True)
        sys.exit(ret)

