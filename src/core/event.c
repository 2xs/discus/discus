/**
 * Allocate or free a memoires space for ethernet packet raw.
 * @author Damien Riquet
 */

#include "platform.h"
#include "core.h"
#include "event.h"
#include "types.h"

void * alloc_ethernet_packet_raw_args_s()
{
    return mem_malloc(sizeof(struct ethernet_packet_raw_args_s));
}

void free_ethernet_packet_raw_args_s(void * args)
{
    struct ethernet_packet_raw_args_s * data = (struct ethernet_packet_raw_args_s *) args;

    /* First, free the bitstream */
    free_bitstream(&data->payload);

    /* Then, free the structure itself */
    mem_free(args);
}
