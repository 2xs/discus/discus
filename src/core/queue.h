#ifndef QUEUE_H
#define QUEUE_H
/**
* queue.h
* Purpose: Provides a way to store event to be processed
*/

/* --------- Queue structure ---------- */

/**
*  Initialize a queue given parameter
*  with the head and count equals to zero
*
*  @param queue to initialze
*/
void queue_init(queue_s * head_queue);


/**
    Push a new event into the queue

    @param queue The queue
    @param handler_func The function handling this event
    @param free_fund The function that frees arguments
    @param args Arguments for this event
    @return Whether or not (1 or 0) the event has been added to the queue
*/
short queue_push(queue_s * head_queue, process_event handler_func, free_args free_func, void * args);

/**
    Return an event from the queue

    @param queue The queue
    @return Number of event popped (0 or 1)
*/
short queue_pop(queue_s * head_queue, event_s * event);

/**
    Return whether of not the queue is full

    @param queue The queue
    @return 1 if the queue is full, 0 otherwise
*/
static inline short queue_full(queue_s * head_queue)
{
    return head_queue->count == MAX_QUEUE_SIZE;
}

/**
 * Displays inforation contains into given queue
 *
 * @param queue - queue to be print
 */
void display_queue(queue_s * head_queue);



/* --------- Delayed queue structure ---------- */
/* Structure to store delayed queue entry */
#define DELAYED_BUSY 0
#define DELAYED_FREE 1

/**
    Initialize a delayed queue
*/
void dqueue_init(dqueue_s * head_dqueue);

delayed_event_s * dqueue_insert(dqueue_s * head_dqueue, int delay);

/**
    Return whether of not the queue is full

    @param queue The queue
    @return 1 if the queue is full, 0 otherwise
*/
static inline short dqueue_full(dqueue_s * head_dqueue)
{
    return head_dqueue->count == MAX_DELAYED_SIZE;
}

/**
    Process the delayed queue and insert event into the main queue
    when the delay has been observed

    @param delayed The delayed queue
    @param queue The main queue
    @return The number of event moved from the delayed queue to the main queue
*/
int process_dqueue(dqueue_s * head_dqueue, queue_s * head_queue);

#endif	/* QUEUE_PRIVATE_H */
