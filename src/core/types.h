#ifndef TYPES_H
#define TYPES_H

/* This file contains the different data types available using Discus Engine */

/* Bitstream are a sequence of bits
 * They are managed using a smart buffer, that keep tracks of usage
 * When a smart buffer detects that this buffer is now useless, it frees it
 */


/* A smart buffer is the structure where the data is stored
 * It also managed when to frees the space */

#define BUFFER_ALLOCATED 1
#define BUFFER_STATIC 0


/* --------- Smart buffer functions ---------- */

/**
 * This function creates a smart buffer containing data
 * @param to Where to store the smart buffer
 * @param data Where is stored the data
 * @param size Size (in bits) of the data
 * @return The smart buffer, if to == NULL, it will allocate memory
 * otherwise, it considers data is already placed at to + sizeof(int)
 */
smart_buffer_s * create_buffer(void * to, void * data, unsigned int size);

/* Usage update functions:
 * - user : increase usage
 * - free : decrease and free the buffer if necessary
 */

/**
 * Just `buffer->usage += 1`.
 *
 * @param buffer The buffer to update.
 */
void use_buffer(smart_buffer_s * buffer);

/**
 * Just `buffer->usage -= 1`. Although it's supposed
 * to free malloced buffers, at first glance malloc is 
 * never called (i.e. `buffer->allocated` never equals 
 * `BUFFER_ALLOCATED`). See `NOTES.md` for details.
 *
 * @param buffer The buffer to update.
 */
void free_buffer(smart_buffer_s * buffer);


/* --------- Bitstreams functions ---------- */

/* Creates a bitstream from a smart buffer
 * @param to Where to store the bitstream structure
 * @param buffer The smart buffer
 * @param size Size (in bits) of the data
 * @param offset Offset in the smart buffer (in bits)
 * if to == NULL, it will allocate memory
 * otherwise, it places the data at to
 */
bitstream_s * create_bitstream(void * to, smart_buffer_s * buffer, unsigned int size, unsigned int offset);

/**
 * Free a bitstream. Should be used only on bitstream allocated 
 * using create_bitstream for example.
 *
 * @param bitstream The bitstream to free.
 */
void free_bitstream(bitstream_s * bitstream);

#endif /* TYPES.H */
