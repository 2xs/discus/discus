/**
 * Initializes events queues, pops events
 * and executes related handlers, provides
 * buffers picked from the buffers list.
 * @author Damien Riquet
 */

#include "platform.h"
#include "core.h"
#include "queue.h"
#include "event.h"
#include "engine.h"
#include "types.h"

#define PACKET_SMART_BUFFER_SIZE (PACKET_SIZE + sizeof(smart_buffer_s))

/* Global variables */
queue_s queue;
dqueue_s dqueue;
unsigned char packet_buffer[PACKET_SMART_BUFFER_SIZE * PACKET_NB];

void engine_init(void)
{
    queue_init(&queue);
    dqueue_init(&dqueue);
}

void engine_stop(void)
{
}

void engine_process(void)
{
    event_s event;

    /* we process event */
    if (queue_pop(&queue, &event))
    {
        event.handler(event.args);
    }
}

smart_buffer_s * engine_get_buffer(void)
{
    smart_buffer_s * buffer;
    unsigned long i;
    for (i = 0; i < PACKET_NB; ++i)
    {
        buffer = (smart_buffer_s *) (packet_buffer + i * PACKET_SMART_BUFFER_SIZE);
        if (buffer->usage == 0)
            return buffer;
    }

    return NULL;
}

