/**
 * Used to manage a event queue.
 * @author Damien Riquet
 */

#include "platform.h"
#include "core.h"
#include "queue.h"
#include "event.h"


/* --------- Queue structure ---------- */

void queue_init(queue_s * head_queue)
{
    head_queue->head = head_queue->count = 0;
}

short queue_push(queue_s * head_queue, process_event handler_func, free_args free_func, void * args)
{
    int idx;
    event_s * event;

    /* Check whether or not there is place for an element in the queue */
    if (queue_full(head_queue))
    {
        /* The queue is full, we cannot add an element */
        return 0;
    }

    /* Copy the content of the event into the queue */
    idx = (head_queue->head + head_queue->count++) % MAX_QUEUE_SIZE;
    event = (&head_queue->content[idx]);

    event->handler = handler_func;
    event->free = free_func;
    event->args = args;

    return 1;
}

short queue_pop(queue_s * head_queue, event_s * event)
{
    unsigned int old_head = head_queue->head;

    /* Check whether or not there is an element in the queue */
    if (queue_empty(head_queue))
    {
        /* The queue is empty, there is no event */
        return 0;
    }

    /* Update the queue and return the event associated */
    head_queue->count -= 1;
    head_queue->head = (head_queue->head + 1) % MAX_QUEUE_SIZE;

    event->handler = head_queue->content[old_head].handler;
    event->free = head_queue->content[old_head].free;
    event->args = head_queue->content[old_head].args;

    return 1;
}

void display_queue(queue_s * head_queue)
{
    log_char(1, "queue: ");
    log_uint(1, head_queue->count);
    log_char(1, " elements - ");
    log_uint(1, head_queue->head);
    log_char(1, "\n");
}


/* --------- Delayed queue structure ---------- */

void dqueue_init(dqueue_s * head_dqueue)
{
    int i;

    /* Initialize the count */
    head_dqueue->count = 0;

    /* Set all the delayed event to free */
    for (i = 0; i < MAX_DELAYED_SIZE; ++i)
        head_dqueue->content[i].free = DELAYED_FREE;
}


delayed_event_s * dqueue_insert(dqueue_s * head_dqueue, int delay)
{
    int i = 0;

    /* Check whether or not there is place for an element in the queue */
    if (dqueue_full(head_dqueue))
    {
        /* The queue is full, we cannot add an element */
        return NULL;
    }

    /* Update the queue */
    head_dqueue->count += 1;

    /* Look for a delayed event free */
    for (i = 0; i < MAX_DELAYED_SIZE; ++i)
    {
        if (head_dqueue->content[i].free == DELAYED_FREE)
        {
            discus_time now = current_time();

            /* Update this delayed event */
            head_dqueue->content[i].delay = now + (discus_time) delay;
            head_dqueue->content[i].free = DELAYED_BUSY;

            return &head_dqueue->content[i];
        }
    }

    return NULL;
}


int process_dqueue(dqueue_s * head_dqueue, queue_s * head_queue)
{
    int n = 0;
    discus_time now = current_time();
    delayed_event_s * devent = head_dqueue->content;
    event_s * event;


    /* Look for delayed event which can be moved to the main queue */
    while (devent < head_dqueue->content + MAX_DELAYED_SIZE)
    {
        if (devent->free == DELAYED_BUSY && devent->delay < now)
        {
            event = &devent->event;
            /* We can move this event into the main queue */
            if (!queue_push(head_queue, event->handler, event->free, event->args))
            {
                /* It means that we cannot insert this event into the queue
                 * So we need to verify if we can free its arguments */
                devent->event.free(devent->event.args);
            }


            head_dqueue->count -= 1;
            devent->free = DELAYED_FREE;
            n += 1;
        }
        devent++;
    }

    return n;
}
