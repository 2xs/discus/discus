#include "platform.h"
#include "core.h"
#include "types.h"

smart_buffer_s * create_buffer(void * to, void * data, unsigned int size)
{
    smart_buffer_s * sb;
    unsigned int byte_size = size / 8;

    /* Manage the to pointer */
    if (to == NULL) {
        /* We need to allocate some memory */
        sb = mem_malloc(sizeof(int) * 2 + byte_size);
        if (sb == NULL) return NULL;

        sb->allocated = BUFFER_ALLOCATED;
    } else {
        /* The smart buffer is already at the correct place */
        sb = (smart_buffer_s *)to;
        sb->allocated = BUFFER_STATIC;
    }

    /* Copy the content of data into the buffer */
    mem_copy(sb->buffer, data, byte_size);

    /* Initialization */
    sb->usage = 0;
    sb->size = size;

    return sb;
}

void use_buffer(smart_buffer_s * buffer)
{
    buffer->usage += 1;
}

void free_buffer(smart_buffer_s * buffer)
{
    if (buffer->usage >= 1)
        buffer->usage -= 1;

    if (buffer->usage == 0 && buffer->allocated == BUFFER_ALLOCATED)
    {
        /* The smart buffer can be freed */
        mem_free(buffer);
    }
}

bitstream_s * create_bitstream(void * to, smart_buffer_s * buffer, unsigned int size, unsigned int offset)
{
    bitstream_s * bitstream;

    /* Manage the to pointer */
    if (to == NULL) {
        /* We need to allocate some memory */
        bitstream = mem_malloc(sizeof(bitstream_s));
        if (bitstream == NULL) return NULL;
    } else {
        bitstream = (bitstream_s *)to;
    }

    /* Initialize data */
    bitstream->smart_buffer = buffer;
    bitstream->size = size;
    bitstream->offset = offset;
    use_buffer(buffer);

    return bitstream;
}

void free_bitstream(bitstream_s * bitstream)
{
    free_buffer(bitstream->smart_buffer);
}

unsigned extract_unsigned_from_bitstream(bitstream_s * bitstream, unsigned int start, unsigned int end)
{
    unsigned res;
    unsigned int i, offset;
    unsigned char * res_ptr = (unsigned char *) &res;
    unsigned int buff_len = sizeof(int);

    /* Initializing variables */
    offset = bitstream->offset;

    /* Copying content */
    for (i = 0; i < buff_len; ++i)
        res_ptr[buff_len - i - 1] = bitstream->smart_buffer->buffer[offset + i];

    /* Shift if necessary */
    /* First, left shift */
    if (start != 0)
        res <<= start;

    if ((end + 1 - start) != buff_len * 8)
        res >>= (buff_len * 8 - end - 1 + start);

    return res;
}

uint64_t extract_long_unsigned_from_bitstream(bitstream_s * bitstream, unsigned int start, unsigned int end)
{
    uint64_t res;
    unsigned int buff_len, res_len, offset_buffer;
    unsigned int rel_start;
    unsigned char * b;

    /* Initialize variables */
    buff_len = sizeof(uint64_t);
    res_len = end - start + 1;

    /* offset_buffer : offset (in bytes) for the smart buffer */
    offset_buffer = (bitstream->offset + start) >> 3;

    /* Relative (to the read uint64_t) start and end */
    rel_start = start - (offset_buffer * 8 - bitstream->offset);

    /* Read the uint64_t and manage indianness */
    b = bitstream->smart_buffer->buffer + offset_buffer;


    /*res = (((uint64_t) b[0]) << 56) | (((uint64_t) b[1]) << 40) | ((unsigned long) (b[2] << 24)) | 
        ((unsigned long) (b[3] << 8)) | (b[3] >> 8) | (b[2] >> 24) | (((uint64_t) b[1]) >> 40) |
        (((uint64_t) b[0]) >> 56);*/

     /* Read the uint64_t and manage indianness */
        res = *((uint64_t *) (bitstream->smart_buffer->buffer + offset_buffer));

	res = (res << 56) | ((res & 0xFF00) << 40) | ((res & 0xFF0000) << 24) | ((res & 0xFF000000) << 8) |
		              ((res >> 8) & 0xFF000000) | ((res >> 24 ) & 0xFF0000) | ((res >> 40) | 0xFF00) | (res >> 56);

    /* Left and right shift */
    res <<= rel_start;
    res >>= (buff_len * 8 - res_len);

    return res;
}

bitstream_s * extract_bitstream_from_bitstream(bitstream_s * src, bitstream_s * dst, unsigned start, unsigned int end)
{
    /* First check if the source bitstream is long enough */
    if (end < start)
        return NULL;

    /* Copy data into the new bitstream */

    dst->smart_buffer = src->smart_buffer;

    /* Use this smart buffer */
    use_buffer(dst->smart_buffer);

    /* Fill data */
    dst->size = end - start;
    dst->offset = src->offset + start;

    return dst;
}

void init_ipv4_addr(ipv4_addr_s * s, uint64_t address, unsigned short mask)
{
    s->s_addr = address;
    s->s_mask = mask;
}

short ipv4_addr_in_network(ipv4_addr_s * addr, ipv4_addr_s * network)
{
    uint64_t m_addr, m_net;

    /* Compute the masked value for the address and the network. */
    /* `addr->s_mask` could be named `addr->s_cidr`. */
    /* It would need to be shifted back to the left but it does not matter. */
    m_addr = addr->s_addr >> (32 - addr->s_mask);
    m_net = network->s_addr >> (32 - network->s_mask);

    return m_addr == m_net;
}

ipv4_addr_s create_ipv4_addr(unsigned char b1, unsigned char b2, unsigned char b3, unsigned char b4, unsigned mask)
{
    ipv4_addr_s ret;
    ret.s_addr = ((uint64_t) (b1 << 24) + ((unsigned long) (b2 << 16)) + ((unsigned long) (b3 << 8))) + b4;
    ret.s_mask = mask;
    return ret;
}

int addr_comp(ipv4_addr_s * a, ipv4_addr_s * b)
{
    if (a->s_addr == b->s_addr && a->s_mask == b->s_mask)
        return 1;
    return 0;
}

void display_addr(ipv4_addr_s * a)
{
    log_ip((a->s_addr >> 24), (a->s_addr >> 16) & 0xFF, (a->s_addr >> 8) & 0xFF, a->s_addr & 0xFF);
}
