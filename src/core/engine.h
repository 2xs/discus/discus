#ifndef ENGINE_H
#define ENGINE_H

/**
  Initializes the events queue and 
 * the delayed events queue. /!\ At 
 * first glance the delayed events 
 * queue is never used.
 */
void engine_init(void);

/**
 * No operation.
 */
void engine_stop(void);

/**
 * If the events queue is not empty,
 * pops the next event and calls its
 * event handler.
 */
void engine_process(void);

#endif /* ENGINE_H */
