#include "platform.h"

void alert(char * category, char * message)
{
    log_char(1, "[ALERT][");
    log_char(1, category);
    log_char(1, "]");
    log_char(1, message);
    log_char(1, "\n");
}
