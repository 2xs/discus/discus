#ifndef CORE_H
#define CORE_H

#include <stdint.h>

#include <platform.h>

/**
 * This structure represents an IPv4 address
 */
typedef struct {
    uint64_t s_addr;
    unsigned int s_mask;
} ipv4_addr_s;

/**
 * This structure represents an smart buffer
 */
typedef struct {
    unsigned int allocated:1;
    unsigned int usage:3;
    unsigned int size;
    unsigned char buffer[];
} smart_buffer_s;

/**
 *  This structure represents an bitstream
 */
typedef struct {
    smart_buffer_s *smart_buffer;
    unsigned int size;
    unsigned int offset;
} bitstream_s;


/* --------- Smart buffer functions ---------- */

/**
 * This function creates a smart buffer containing data
 * @param to Where to store the smart buffer
 * @param data Where is stored the data
 * @param size Size (in bits) of the data
 * @return The smart buffer
 *
 * if to == NULL, it will allocate memory
 * otherwise, it considers data is already placed at to + sizeof(int)
 */
smart_buffer_s * create_buffer(void * to, void * data, unsigned int size);

/* --------- Bitstreams Extraction ---------- */

/**
 * Extracts a small sequence of bits of
 * `min(sizeof(int) * 8, end + 1 - start)`
 * bits long from the provided bitstream.
 *
 * @param bitstream The bitstream from which to extract the small bitstream.
 * @param start The start offset of the small bitstream relative to the passed bitstream.
 * @param end The end offset of the small bitstream relative to the passed bitstream.
 * @return The small sequence of bits.
 */
unsigned extract_unsigned_from_bitstream(bitstream_s * bitstream, unsigned int start, unsigned int end);

/**
 * Extracts a small sequence of bits of
 * `min(sizeof(uint64_t) * 8, end + 1 - start)`
 * bits long from the provided bitstream.
 *
 * Additionnally, it reverses the read sequence
 * of bytes in order to deal with endianness.
 * Hence, this function is probably used to read
 * IP addresses.
 *
 * Side note: `res <<= rel_start;` is possibly
 * useless since, at first glance, `rel_start`
 * is always 0.
 *
 * @param bitstream The bitstream from which to extract the small bitstream.
 * @param start The start offset of the small bitstream relative to the passed bitstream.
 * @param end The end offset of the small bitstream relative to the passed bitstream.
 * @return The small sequence of bits.
 */
uint64_t extract_long_unsigned_from_bitstream(bitstream_s * bitstream, unsigned int start, unsigned int end);

/**
 * Extracts a bitstream from a bigger one.
 * More precisely, the buffer of the source
 * bitstream is copied to the buffer of the
 * destination bitstream, and the "usage"
 * field of the buffer is incremented.
 *
 * @param src The big bitstream.
 * @param dst The smaller bitstream.
 * @param start The start offset of the small bitstream relative to the passed bitstream.
 * @param end The end offset of the small bitstream relative to the passed bitstream.
 * @return The small bitstream.
 */
bitstream_s * extract_bitstream_from_bitstream(bitstream_s * src, bitstream_s * dst, unsigned start, unsigned int end);

/* --------- Network types functions ---------- */

/**
 * Sets the fields `s_addr` and `s_mask`
 * of the passed struct to the passed values.
 *
 * @param s An IPV4 address.
 * @param address
 * @param mask
 */
void init_ipv4_addr(ipv4_addr_s * s, uint64_t address, unsigned short mask);

/**
 * Creates an IPV4 address based
 * on four individual bytes.
 *
 * @param b1 The first byte.
 * @param b2 The second byte.
 * @param b3 The third byte.
 * @param b4 The fourth byte.
 * @param mask The number of bits related to the network address.
 * @return The new IPV4 address.
 */
ipv4_addr_s create_ipv4_addr(unsigned char b1, unsigned char b2, unsigned char b3, unsigned char b4, unsigned mask);

/**
 * Compares IPV4 network addresses.
 *
 * @param addr An IPV4 address.
 * @param network An IPV4 address.
 * @return The result of the comparison.
 */
short ipv4_addr_in_network(ipv4_addr_s * addr, ipv4_addr_s * network);

/**
 * Compares IPV4 addresses.
 *
 * @param a An IPV4 address.
 * @param b An IPV4 address.
 * @return 1 if `s_addr` and `s_mask` fields are equals, 0 otherwise.
 */
int addr_comp(ipv4_addr_s * a, ipv4_addr_s * b);

/**
 * Displays an IPV4 address.
 *
 * @param a An IPV4 address.
 */
void display_addr(ipv4_addr_s * a);

/* --------- Bitstreams functions ---------- */

/**
 * Creates a bitstream from a smart buffer
 * @param to Where to store the bitstream structure
 * @param buffer The smart buffer
 * @param size Size (in bits) of the data
 * @param offset Offset in the smart buffer (in bits)
 * if to == NULL, it will allocate memory
 * otherwise, it places the data at to
 */
bitstream_s * create_bitstream(void * to, smart_buffer_s * buffer, unsigned int size, unsigned int offset);

/**
 * Free a bitstream
 * Should be used only on bitstream allocated
 * using create_bitstream for example.
 */
void free_bitstream(bitstream_s * bitstream);

/**
 * Define a function type
 * process_rules will be implanted by every security rules.
 */
typedef void (*process_event)(void *args);
typedef void (*free_args)(void *args);

/**
 * The event with ID 0 is special:
 * it corresponds to the only event raised from the engine
 */
struct ethernet_packet_raw_args_s {
        bitstream_s payload;
};

/**
 * That structure defines an event.
 */
typedef struct {
    process_event handler;
    free_args free;
    void *args;
} event_s;

/**
 * Allocate/free initial event arguements
 */
void handle_ethernet_packet_raw(void * args);

/**
 * Allocate a memoires space the size of ethernet packet raw struct.
 * @return The reserved memory space.
 */
void * alloc_ethernet_packet_raw_args_s(void);

/**
 * Call free_bitstream to free the ethernet packet playload and the enclosing struct.
 * @param The ethernet packet to free
 */
void free_ethernet_packet_raw_args_s(void * args);

#define MAX_QUEUE_SIZE 50
#define MAX_DELAYED_SIZE 10

/**
 * Queue structure
 */
typedef struct {
    event_s content[MAX_QUEUE_SIZE];
    unsigned int head, count;
} queue_s;

/**
 * Structure to store delayed queue entry
 */
typedef struct {
    event_s event;
    short free;
    discus_time delay;
} delayed_event_s;

typedef struct {
    delayed_event_s content[MAX_DELAYED_SIZE];
    unsigned int count;
} dqueue_s;

/**
 * Push a new event into the queue
 * @param queue The queue
 * @param handler_func The function handling this event
 * @param free_fund The function that frees arguments
 * @param args Arguments for this event
 * @return Whether or not (1 or 0) the event has been added to the queue
 */
short queue_push(queue_s * queue, process_event handler_func, free_args free_func, void * args);

/* This module is here to declare two function related to regular expressions : init and free
 * These expressions will be generated using the discus input file, elsewhere
 */

extern void regexp_init(void);
extern void regexp_free(void);

#define PACKET_SIZE 1538
#define PACKET_NB 25

/**
 * current queue
 */
extern queue_s queue;
/**
 * current delay queue
 */
extern dqueue_s dqueue;

/**
 * Tries to find a free buffer in the
 * packet buffer list and returns it.
 * @return A free buffer or NULL if
 * there is no buffer available.
 */
smart_buffer_s * engine_get_buffer(void);

#define TABLE_INITIAL_DATA_ROW 10

typedef short (*func_tst) (void *entry);
typedef func_tst(purge_candidate_fun);
typedef func_tst(delete_candidate_fun);
typedef func_tst(free_entry_candidate_fun);

struct table_s {
	purge_candidate_fun is_purge_candidate;
	delete_candidate_fun is_delete_candidate;
	free_entry_candidate_fun free_entry_candidate;
	struct node_s *head_node;
	unsigned int size_of_entry;
	unsigned int entry_count;
};

#define YES 1
#define NO  0

struct entry_flags_s {
    unsigned char busy            : 1;
    unsigned char purge_candidate : 1;
    unsigned char unused          : 6;
};

/* Iterator mechanism */
struct iterator_s {
    unsigned int table_id;
	struct node_s * current_entry;
};
/**
 * Create a table, to store sizeof entry, purge, delete, and
 * free entry functions. This functions will keep a pointer
 * to the first entry of our linked list.
 * 
 * @param table_id Id of the table.
 * @param size_of_entry The size of entry.
 * @param purge_fun How the table will be purged.
 * @param delete_fun How an item of this list will deleted.
 * @param free_entry_fun Precise how free_table will free each entry.
 */
void create_table(unsigned int table_id, unsigned int row_size,
		purge_candidate_fun purge_fun,
		delete_candidate_fun delete_fun,
		free_entry_candidate_fun free_entry_fun);

/**
 * Insert value into given table.
 * @param The table id
 * @param The value to inset into table
 */
void * insert_entry(unsigned int table_id);

/* Iterator operations */

/**
 * Initialize an iterator
 * @param it the iterator
 * @param table_id id of the table
 */
void init_iterator(unsigned int table_id, struct iterator_s * it);

/**
 * Look for the next entry for an iterator
 * @param it the iterator
 * @return a pointer to the next entry or NULL
 */
void * iterator_next(struct iterator_s * it);

/**
 * Show the parameter in alert box.
 *
 * @param category type of alert box.
 * @param message to be print in the alert box.
 */
void alert(char * category, char * message);

/**
  Initializes the events queue and 
 * the delayed events queue. /!\ At 
 * first glance the delayed events 
 * queue is never used.
 */
void engine_init(void);

/**
 * No operation.
 */
void engine_stop(void);

/**
 * If the events queue is not empty,
 * pops the next event and calls its
 * event handler.
 */
void engine_process(void);

extern void table_init(void);


/**
 * Return whether of not the queue is empty
 *
 * @param queue The queue
 * @return 1 if the queue is empty, 0 otherwise
 */
static inline short queue_empty(queue_s * head_queue)
{
	    return head_queue->count == 0;
}

#endif /* CORE_H */
