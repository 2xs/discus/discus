#ifndef PLATFORM_H
#define PLATFORM_H

/**
 * Hardware abstractions are sets of routines
 * in software that emulate some platform-specific details
 * @author Clément Boin
 */

#ifndef NULL
#define NULL ((void *) 0)
#endif

/**
 * We define these the same for all machines.
 */
#define EXIT_FAILURE 1 /* Faiming exit status. */
#define EXIT_SUCCESS 0 /* Successful exit status. */

typedef unsigned long discus_time;

/**
 * Standard I/O routines
 */

/**
 * Loads the data from the given locations,
 * converts them to character string equivalents
 * and writes the results to a variety of sinks
 * @param Alert level.
 * @param The string to display
 * @return The number of characters printed
 */
int log_char(int lvl, const char *s);

int log_int(int lvl, const int);

int log_addr(int lvl, const void *);

int log_hex(int lvl, const unsigned char *);

int log_ip(unsigned long digit1, unsigned long digit2, unsigned long digit3, unsigned long digit4);

int log_uint(int lvl, const unsigned int);

/**
 * Standard memory routines
 */

/**
 * Copies n bytes from memory area source
 * to memory area destination.
 * The memory areas must not overlap.
 * @param The destination memory area.
 * @param The source memory area.
 * @param The number of bytes to copy.
 */
void *mem_copy(void *dest, const void *src, unsigned int n);

/**
 * Frees the memory space pointed to by pointer.
 * If pointer is NULL, no operation is performed.
 * @param Pointer to the memory area to free.
 */
void mem_free(void *ptr);

/**
 * Allocates size bytes and returns a pointer to the allocated memory.
 * The memory is not initialized. If size is 0, then memory_alloc()
 * returns either NULL, or a unique pointer value
 * that can later be successfully passed to free().
 * @param Size bytes to allocated.
 * @return A pointer to the allocated memory.
 */
void *mem_malloc(unsigned int size);

/**
 * Changes the size of the memory block pointed to by pointer
 * to size bytes.
 * Get current time
 * @return The current time
 */
discus_time current_time(void);


/**
 * The function cause normal process termination.
 * @return The function does not return.
 */
void clear_exit(int status);

/**
 * Fill memory with a constant byte.
 * @param Pointer to the block of memory to fill.
 * @param Value to be set.
 * @param Number of bytes to be set to the value
 */
void *mem_memset(void *ptr, int value, unsigned int n);

void *mem_realloc(void *ptr, unsigned int size);

#define FILTER_EXP "tcp"

/**
 * Initializes interface for network capture.
 *
 * @param The network interface.
 */
void iface_init(const char *);

/**
 * Open pcap file for reading.
 *
 * @param pcap file.
 */
void pcap_single_init(const char *);

/**
 * Stop the network capture.
 */
void target_stop(void);

/**
 * Read next packet from capture device.
 *
 * @return interger more than zero on sucess,
 * or equals to, or less than zero if an error
 * occurred, or if no packets were read.
 */
int read_next_packet(void);

#endif /* PLATFORM_H */
