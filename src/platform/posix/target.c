/**
 * Established the network capture.
 * @author Damien riquet
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pcap.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "platform.h"
#include "core.h"

pcap_t * handle;

void iface_init(const char *iface)
{
    char errbuf[PCAP_ERRBUF_SIZE]; /* Error string */
    uint32_t netaddr, netmask; /* Address and mask of the network */
    struct bpf_program filter_exp; /* Compiled filter expression */

    /* First, fetch the network address and mask */
    if (pcap_lookupnet(iface, &netaddr, &netmask, errbuf) == -1)
    {
        fprintf(stderr, "Can't get netmask for device %s: %s\n", iface, errbuf);
        netaddr = netmask = 0;
    }

    /* Create the handler session */
    handle = pcap_open_live(iface, PACKET_SIZE, 1, 0, errbuf);
    if (handle == NULL)
    {
        fprintf(stderr, "Couldn't open device %s: %s\n", iface, errbuf);
    }

    /* Compile the filter expression */
    if (pcap_compile(handle, &filter_exp, FILTER_EXP, 1, netaddr) == -1)
    {
        fprintf(stderr, "Couldn't parse filter expression %s: %s\n", FILTER_EXP, pcap_geterr(handle));
        exit(0);
    }

    /* Set the filter */
    if (pcap_setfilter(handle, &filter_exp) == -1)
    {
        fprintf(stderr, "Could't install filter %s: %s\n", FILTER_EXP, pcap_geterr(handle));
        exit(0);
    }

}

void pcap_single_init(const char *file)
{
	FILE *fildes;
	if ((fildes = fopen(file, "r")) == NULL)
	{
		perror("pcap single file:");
		exit(EXIT_FAILURE);
	}

	char errbuf[PCAP_ERRBUF_SIZE];
	if ((handle = pcap_fopen_offline(fildes, errbuf)) == NULL)
	{
		fprintf(stderr, "%s\n", errbuf);
		exit(EXIT_FAILURE);
	}
}

void target_stop()
{
    pcap_close(handle);
}

int read_next_packet(void)
{
    const unsigned char *packet;
    struct pcap_pkthdr header;

    packet = pcap_next(handle, &header);
    if (packet != NULL)
    {
        /* Try to insert this packet into the queue */
        smart_buffer_s * buffer = engine_get_buffer();
        struct ethernet_packet_raw_args_s * args;


        if (buffer != NULL)
        {
            /* A smart buffer is available, we can copy the content of the packet in it
             * and create an event associated to the new network packet */
            create_buffer(buffer, (void *) packet, header.caplen * 8);

            /* Allocating the argument structure and initializing its data */
            if ((args = alloc_ethernet_packet_raw_args_s()) == NULL )
            {
                fprintf(stderr, "Couldn't allocated memory space\n");
            } else {
                create_bitstream((void *)&args->payload, buffer, header.caplen * 8, 0);

                /* Now we insert the event into the queue */
                if (!queue_push(&queue, handle_ethernet_packet_raw, free_ethernet_packet_raw_args_s, args))
                {
                    /* The event was not pushed successfully */
                    free_ethernet_packet_raw_args_s(args);
                    buffer->usage = 0; /* Reset the smart buffer */
		}
	   }
	}
    }

	if (packet == NULL)
		return 0;
	return 1;
}
