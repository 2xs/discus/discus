#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "platform.h"

int log_char(int lvl, const char *s)
{
    int i = 0;

    switch (lvl) {
        case 1:
           i = fprintf(stdout, "%s", s);
            break;
        case 2:
            i = fprintf(stderr, "%s", s);
            break;
        default:
            break;
    }

    return i;
}

int log_int(int lvl, const int k)
{
    int i = 0;

    switch (lvl) {
        case 1:
            i = fprintf(stdout, "%d", k);
            break;
        case 2:
            i = fprintf(stderr, "%d", k);
            break;
        default:
            break;
    }

    return i;
}

int log_uint(int lvl, const unsigned int k)
{
	int i = 0;

	switch (lvl)
	{
		case 1:
			i = fprintf(stdout, "%u", k);
			break;
		case 2:
			i = fprintf(stderr, "%u", k);
			break;
		default:
			break;

	}

	return i;
}

int log_addr(int lvl, const void  *addr)
{
    int i = 0;

    switch (lvl) {
        case 1:
            i = fprintf(stdout, "%p", addr);
            break;
        case 2:
            i = fprintf(stderr, "%p", addr);
            break;
        default:
            break;
    }

    return i;
}

int log_hex(int lvl, const unsigned char *data)
{
    int i = 0;

    switch (lvl) {
        case 1:
            i = fprintf(stdout, "%02x ", *data);
            break;
        case 2:
            i = fprintf(stderr, "%02x ", *data);
            break;
        default:
            break;
    }

    return i;
}

int log_ip(unsigned long digit1, unsigned long digit2, unsigned long digit3, unsigned long digit4)
{
    int i = 0;

    printf("%3lu.%3lu.%3lu.%3lu", digit1, digit2, digit3, digit4);

    return i;
}

void *mem_copy(void *dest, const void *src, unsigned int n)
{
    return memcpy(dest, src, n);
}

void mem_free(void *ptr)
{
    free(ptr);
}

void *mem_malloc(unsigned int size)
{
    return malloc(size);
}

discus_time current_time(void)
{
    return (discus_time) time(NULL);
}

void clear_exit(int status)
{
    exit(status);
}

void *mem_memset(void *ptr, int value, unsigned int n)
{
    return memset(ptr, value, n);
}

void *mem_realloc(void *ptr, unsigned int size)
{
    return realloc(ptr, size);
}
