/**
 * Program entry point and manages the user input.
 * @author Damien Riquet
 */

#include <stdio.h>
#include <pcap.h>
#include <getopt.h>

#include "platform.h"
#include "core.h"

int main(int argc, char *argv[])
{
	int opt = 0;
	static struct option long_options[] = {
		{"iface", required_argument, 0, 'i'},
		{"pcap-single", required_argument, 0, 'r'},

		{0, 0, 0, 0}
	};

	int long_index = 0;
	while ((opt = getopt_long_only(argc, argv, "i:r:", long_options, &long_index)) != -1)
	{
		switch (opt)
		{
		case 'i':
			iface_init(optarg);
			break;
		case 'r':
			pcap_single_init(optarg);
			break;
		default:
			fprintf(stderr, "nope");
			break;
		}
	}

	engine_init();
	table_init();
	regexp_init();

	while (read_next_packet() != 0)
		while (!queue_empty(&queue))
		{
			engine_process();
		}

	engine_stop();
	target_stop();
	regexp_free();

	return EXIT_SUCCESS;
}
