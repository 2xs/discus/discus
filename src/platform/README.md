# What is?
This file contains all the definitions that are dependent on a particular platform
The files present in the folder *includes* list all the functionalities that must be written to allow the operation of discus.

# Make the librairy
To generate a library that enables connections between your system and DISCUS, you must write all the functionalities present in the file platform.h, and also that of target.h.
The actual implementation should be in a folder that has the name of the platform.

# Folder description
- includes: The discus feature that is platform dependent.
- posix: 
